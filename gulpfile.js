var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var cachebust   = require('gulp-hash-cachebuster');
var uglify      = require('gulp-uglify');
var babel       = require('gulp-babel');
var rename      = require("gulp-rename");

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("pages/scss/*.scss")
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("./build/css"))
        .pipe(browserSync.stream());
});

gulp.task('cachebust', function() {
    return gulp.src('./build/*.html')
        .pipe(cachebust())
        .pipe(gulp.dest('./build/'))
})

gulp.task('copy-html', function() {
    return gulp.src('./pages/*.html')
        .pipe(gulp.dest('./build/'))
        .pipe(browserSync.stream());
})

gulp.task('copy-json', function() {
    return gulp.src('./pages/*.json')
        .pipe(gulp.dest('./build/'))
        .pipe(browserSync.stream());
})

gulp.task('copy-fonts', function() {
    return gulp.src('./pages/fonts/*')
        .pipe(gulp.dest('./build/fonts/'))
        .pipe(browserSync.stream());
})

gulp.task('copy-scripts', function() {
    return gulp.src('./pages/scripts/*')
        .pipe(uglify())
        .pipe(gulp.dest('./build/scripts/'))
        .pipe(browserSync.stream());
})

gulp.task('copy-images', function() {
    return gulp.src('./pages/images/*')
        .pipe(gulp.dest('./build/images/'))
        .pipe(browserSync.stream());
})

gulp.task('transpile-scripts', function() {
    return gulp.src('./pages/scripts/cumbria-scripts.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(rename('cumbria-scripts-ie11.js'))
        .pipe(gulp.dest('./build/scripts/'));
})

// Static Server + watching scss/html files
gulp.task('serve', gulp.series(['copy-html','copy-json','copy-images','copy-scripts','copy-fonts','sass','transpile-scripts','cachebust'], function() {

    browserSync.init({
        server: "./build/",
		ghostMode: false
    });

    gulp.watch("pages/scss/*.scss", gulp.series(['sass','cachebust']));
    gulp.watch("pages/*.html").on('change', gulp.series(['copy-html','cachebust']));
    gulp.watch("pages/*.json").on('change', gulp.series(['copy-json','cachebust']));
    gulp.watch("pages/images/*").on('change', gulp.series(['copy-images','cachebust']));
    gulp.watch("pages/scripts/*.js").on('change', gulp.series(['copy-scripts','transpile-scripts','cachebust']));
}));

gulp.task('default', gulp.series('serve'));