## Install

clone the repo,

install gulp

```npm install --global gulp-cli```

then the project

```npm install```

then

```gulp```

to run the local server @ [localhost:3000](http://localhost:3000)

## Gulp

gulp runs a series of watch items in the `page` source directory, minifies/uglifies and copies all files to the `build` dir then spins up/spits them out to a server for display. If any html, css or js files change then gulp runs the uglifier and copies all files to the distribution `build` dir again. Gulp also creates a ie-11-friendly version of `cumbria-scripts.js` which is not used on the site due to not supporting that browser.

## Where to start

all the files in the `pages` dir are the files to edit. Files in the `build` dir are the minified, 'finished' versions.

## Other things

gulp doesn't look out for new files or image changes, so to include those quit gulp and run again to update `build`.