// set carousel container
const blocksCarouselContainer = document.querySelector('.carousel-blocks-content')

// initialise slider
const blocksCarousel = new A11YSlider(blocksCarouselContainer, {
    adaptiveHeight: false,
    dots: false,
    container: false,
    swipe: true,
    skipBtn: true,
    swipe: true,
    slidesToShow: 1,
    responsive: {
        500: {
            slidesToShow: 2
        },
        700: {
            slidesToShow: 3
        },
        900: {
            slidesToShow: 3
        }
    }
});