// initialise slider
$('div.carousel-image-only-fade-container').slick({
    slidesToShow: 3,
    dots: false,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1,
          centerMode: true
        }
      }
    ]
  });