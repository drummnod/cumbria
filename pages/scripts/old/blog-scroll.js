$(document).ready(function () {
  
  'use strict';
  
   var c, currentScrollTop = 0,
       navbar = $('.blog-nav-mobile');

   $(window).scroll(function () {
      var a = $(window).scrollTop();
      var b = navbar.height();
     
      currentScrollTop = a;
     
      if (c < currentScrollTop && a > b + b) {
        navbar.removeClass("scrollUp");
      } else if (c > currentScrollTop && !(a <= b)) {
        navbar.addClass("scrollUp");
      }
      c = currentScrollTop;
  });
  
});