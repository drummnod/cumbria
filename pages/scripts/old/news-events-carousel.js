// set carousel container
const newsEventsCarouselContainer = document.querySelector('.carousel-news-events-content')

// initialise slider
const newsEventsCarousel = new A11YSlider(newsEventsCarouselContainer, {
    adaptiveHeight: true,
    dots: false,
    container: false,
    swipe: true,
    skipBtn: true,
    swipe: true,
    slidesToShow: 1,
    responsive: {
        500: {
            slidesToShow: 2
        },
        700: {
            slidesToShow: 3
        },
        900: {
            slidesToShow: 3
        }
    }
});