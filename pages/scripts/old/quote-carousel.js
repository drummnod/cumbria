// set carousel container
const quoteCarouselContainer = document.querySelector('.carousel-quote-desc-container')

// initialise slider
const quoteCarousel = new A11YSlider(quoteCarouselContainer, {
    adaptiveHeight: false,
    dots: false,
    container: false,
    swipe: true,
    skipBtn: true,
    swipe: true,
    slidesToShow: 1
});