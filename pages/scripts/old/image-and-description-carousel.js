// set carousel container
const imageDescriptionCarouselContainer = document.querySelector('.carousel-image-desc-container')

// initialise slider
const imageDescriptionCarousel = new A11YSlider(imageDescriptionCarouselContainer, {
    adaptiveHeight: false,
    dots: false,
    container: false,
    swipe: true,
    skipBtn: true,
    swipe: true,
    slidesToShow: 1,
    responsive: {
        500: {
            slidesToShow: 2
        },
        700: {
            slidesToShow: 3
        },
        900: {
            slidesToShow: 4
        }
    }
});