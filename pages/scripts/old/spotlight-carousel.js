// set images and text carousel containers
const spotlightCarouselContainerImages = document.querySelector('.carousel-spotlight-images')
const spotlightCarouselContainerText = document.querySelector('.carousel-spotlight-content')

// initialise both sliders - one for the images, one for the content. One is set to mimic the other so only need one set of buttons to control both
const spotlightCarouselImages = new A11YSlider(spotlightCarouselContainerImages, {
    adaptiveHeight: false,
    dots: false,
    container: false,
    swipe: true,
    skipBtn: false, // 'skip this carousel' button disabled due to two containers fused together - doesn't skip both so wouldn't skip whole carousel
    prevArrow: $('#spotlightPrevButton'),
    nextArrow: $('#spotlightNextButton'),
    swipe: true
});
const spotlightCarouselText = new A11YSlider(spotlightCarouselContainerText, {
    adaptiveHeight: true,
    dots: false,
    container: false,
    arrows: false,
    skipBtn: false,
    swipe: true
});

// set content slider to listen to images slider for user input and copy - so both sliders move as one carousel
spotlightCarouselContainerImages.addEventListener("beforeChange", function(e) {
    // Get next element
    const nextSlide = e.detail.nextSlide;
    // Get index of the next element
    const nextSlideIndex = Array.from(nextSlide.parentNode.children).indexOf(nextSlide);
    // Tell 2nd slider to move to slide element based off index
    spotlightCarouselText.scrollToSlide(nextSlideIndex);
});

// find height of title box currently shown and move slider control buttons to middle of box
var spotlightCarouselButtonVerticalAlign = function (info) {
    let newHeight = $('.a11y-slider-active .carousel-spotlight-content-title').height();

    $('.carousel-spotlight-content-controls').height(newHeight);
}

spotlightCarouselContainerImages.addEventListener("afterChange", function(e) {
    spotlightCarouselButtonVerticalAlign();
});

function skipCarousel() {
    // 13 == enter key, 32 == spacebar
    return (event.keyCode != 13 && event.keyCode != 32) || document.getElementById('carousel-skipped').focus();
}

 $(document).ready(function(){
    // set up the vertical alignment of slider buttons when font loaded and page ready
    document.fonts.ready.then(function () {
        spotlightCarouselButtonVerticalAlign()
    });
	// if slider exists, set vertical height of slider buttons in case webfonts don't load
    if(spotlightCarouselText) {
        spotlightCarouselButtonVerticalAlign()
    }
    // reset vertical alignment on window resize
    $(window).resize(function() {
        spotlightCarouselButtonVerticalAlign()
    });
});