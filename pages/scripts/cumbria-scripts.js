/* a11y-slider - v0.4.6
* https://github.com/mmahandev/a11y-slider
* Copyright (c) 2021 mmahandev. Licensed MIT */
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):(e=e||self).A11YSlider=t()}(this,(function(){"use strict";var e="undefined"!=typeof globalThis?globalThis:"undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:{};function t(e,t){return e(t={exports:{}},t.exports),t.exports}var i=function(e){return e&&e.Math==Math&&e},n=i("object"==typeof globalThis&&globalThis)||i("object"==typeof window&&window)||i("object"==typeof self&&self)||i("object"==typeof e&&e)||Function("return this")(),r=function(e){try{return!!e()}catch(e){return!0}},s=!r((function(){return 7!=Object.defineProperty({},1,{get:function(){return 7}})[1]})),o={}.propertyIsEnumerable,a=Object.getOwnPropertyDescriptor,l={f:a&&!o.call({1:2},1)?function(e){var t=a(this,e);return!!t&&t.enumerable}:o},u=function(e,t){return{enumerable:!(1&e),configurable:!(2&e),writable:!(4&e),value:t}},c={}.toString,d=function(e){return c.call(e).slice(8,-1)},h="".split,f=r((function(){return!Object("z").propertyIsEnumerable(0)}))?function(e){return"String"==d(e)?h.call(e,""):Object(e)}:Object,p=function(e){if(null==e)throw TypeError("Can't call method on "+e);return e},v=function(e){return f(p(e))},y=function(e){return"object"==typeof e?null!==e:"function"==typeof e},b=function(e,t){if(!y(e))return e;var i,n;if(t&&"function"==typeof(i=e.toString)&&!y(n=i.call(e)))return n;if("function"==typeof(i=e.valueOf)&&!y(n=i.call(e)))return n;if(!t&&"function"==typeof(i=e.toString)&&!y(n=i.call(e)))return n;throw TypeError("Can't convert object to primitive value")},m={}.hasOwnProperty,g=function(e,t){return m.call(e,t)},_=n.document,S=y(_)&&y(_.createElement),w=function(e){return S?_.createElement(e):{}},E=!s&&!r((function(){return 7!=Object.defineProperty(w("div"),"a",{get:function(){return 7}}).a})),A=Object.getOwnPropertyDescriptor,L={f:s?A:function(e,t){if(e=v(e),t=b(t,!0),E)try{return A(e,t)}catch(e){}if(g(e,t))return u(!l.f.call(e,t),e[t])}},O=function(e){if(!y(e))throw TypeError(String(e)+" is not an object");return e},C=Object.defineProperty,k={f:s?C:function(e,t,i){if(O(e),t=b(t,!0),O(i),E)try{return C(e,t,i)}catch(e){}if("get"in i||"set"in i)throw TypeError("Accessors not supported");return"value"in i&&(e[t]=i.value),e}},x=s?function(e,t,i){return k.f(e,t,u(1,i))}:function(e,t,i){return e[t]=i,e},j=function(e,t){try{x(n,e,t)}catch(i){n[e]=t}return t},T="__core-js_shared__",N=n[T]||j(T,{}),M=Function.toString;"function"!=typeof N.inspectSource&&(N.inspectSource=function(e){return M.call(e)});var P,D,I,H=N.inspectSource,B=n.WeakMap,F="function"==typeof B&&/native code/.test(H(B)),R=t((function(e){(e.exports=function(e,t){return N[e]||(N[e]=void 0!==t?t:{})})("versions",[]).push({version:"3.6.4",mode:"global",copyright:"© 2020 Denis Pushkarev (zloirock.ru)"})})),V=0,W=Math.random(),X=function(e){return"Symbol("+String(void 0===e?"":e)+")_"+(++V+W).toString(36)},z=R("keys"),q=function(e){return z[e]||(z[e]=X(e))},G={},U=n.WeakMap;if(F){var Y=new U,$=Y.get,K=Y.has,J=Y.set;P=function(e,t){return J.call(Y,e,t),t},D=function(e){return $.call(Y,e)||{}},I=function(e){return K.call(Y,e)}}else{var Q=q("state");G[Q]=!0,P=function(e,t){return x(e,Q,t),t},D=function(e){return g(e,Q)?e[Q]:{}},I=function(e){return g(e,Q)}}var Z,ee,te={set:P,get:D,has:I,enforce:function(e){return I(e)?D(e):P(e,{})},getterFor:function(e){return function(t){var i;if(!y(t)||(i=D(t)).type!==e)throw TypeError("Incompatible receiver, "+e+" required");return i}}},ie=t((function(e){var t=te.get,i=te.enforce,r=String(String).split("String");(e.exports=function(e,t,s,o){var a=!!o&&!!o.unsafe,l=!!o&&!!o.enumerable,u=!!o&&!!o.noTargetGet;"function"==typeof s&&("string"!=typeof t||g(s,"name")||x(s,"name",t),i(s).source=r.join("string"==typeof t?t:"")),e!==n?(a?!u&&e[t]&&(l=!0):delete e[t],l?e[t]=s:x(e,t,s)):l?e[t]=s:j(t,s)})(Function.prototype,"toString",(function(){return"function"==typeof this&&t(this).source||H(this)}))})),ne=n,re=function(e){return"function"==typeof e?e:void 0},se=function(e,t){return arguments.length<2?re(ne[e])||re(n[e]):ne[e]&&ne[e][t]||n[e]&&n[e][t]},oe=Math.ceil,ae=Math.floor,le=function(e){return isNaN(e=+e)?0:(e>0?ae:oe)(e)},ue=Math.min,ce=function(e){return e>0?ue(le(e),9007199254740991):0},de=Math.max,he=Math.min,fe=function(e,t){var i=le(e);return i<0?de(i+t,0):he(i,t)},pe=function(e){return function(t,i,n){var r,s=v(t),o=ce(s.length),a=fe(n,o);if(e&&i!=i){for(;o>a;)if((r=s[a++])!=r)return!0}else for(;o>a;a++)if((e||a in s)&&s[a]===i)return e||a||0;return!e&&-1}},ve={includes:pe(!0),indexOf:pe(!1)},ye=ve.indexOf,be=function(e,t){var i,n=v(e),r=0,s=[];for(i in n)!g(G,i)&&g(n,i)&&s.push(i);for(;t.length>r;)g(n,i=t[r++])&&(~ye(s,i)||s.push(i));return s},me=["constructor","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","toLocaleString","toString","valueOf"],ge=me.concat("length","prototype"),_e={f:Object.getOwnPropertyNames||function(e){return be(e,ge)}},Se={f:Object.getOwnPropertySymbols},we=se("Reflect","ownKeys")||function(e){var t=_e.f(O(e)),i=Se.f;return i?t.concat(i(e)):t},Ee=function(e,t){for(var i=we(t),n=k.f,r=L.f,s=0;s<i.length;s++){var o=i[s];g(e,o)||n(e,o,r(t,o))}},Ae=/#|\.prototype\./,Le=function(e,t){var i=Ce[Oe(e)];return i==xe||i!=ke&&("function"==typeof t?r(t):!!t)},Oe=Le.normalize=function(e){return String(e).replace(Ae,".").toLowerCase()},Ce=Le.data={},ke=Le.NATIVE="N",xe=Le.POLYFILL="P",je=Le,Te=L.f,Ne=function(e,t){var i,r,s,o,a,l=e.target,u=e.global,c=e.stat;if(i=u?n:c?n[l]||j(l,{}):(n[l]||{}).prototype)for(r in t){if(o=t[r],s=e.noTargetGet?(a=Te(i,r))&&a.value:i[r],!je(u?r:l+(c?".":"#")+r,e.forced)&&void 0!==s){if(typeof o==typeof s)continue;Ee(o,s)}(e.sham||s&&s.sham)&&x(o,"sham",!0),ie(i,r,o,e)}},Me=Array.isArray||function(e){return"Array"==d(e)},Pe=function(e){return Object(p(e))},De=function(e,t,i){var n=b(t);n in e?k.f(e,n,u(0,i)):e[n]=i},Ie=!!Object.getOwnPropertySymbols&&!r((function(){return!String(Symbol())})),He=Ie&&!Symbol.sham&&"symbol"==typeof Symbol.iterator,Be=R("wks"),Fe=n.Symbol,Re=He?Fe:Fe&&Fe.withoutSetter||X,Ve=function(e){return g(Be,e)||(Ie&&g(Fe,e)?Be[e]=Fe[e]:Be[e]=Re("Symbol."+e)),Be[e]},We=Ve("species"),Xe=function(e,t){var i;return Me(e)&&("function"!=typeof(i=e.constructor)||i!==Array&&!Me(i.prototype)?y(i)&&null===(i=i[We])&&(i=void 0):i=void 0),new(void 0===i?Array:i)(0===t?0:t)},ze=se("navigator","userAgent")||"",qe=n.process,Ge=qe&&qe.versions,Ue=Ge&&Ge.v8;Ue?ee=(Z=Ue.split("."))[0]+Z[1]:ze&&(!(Z=ze.match(/Edge\/(\d+)/))||Z[1]>=74)&&(Z=ze.match(/Chrome\/(\d+)/))&&(ee=Z[1]);var Ye=ee&&+ee,$e=Ve("species"),Ke=function(e){return Ye>=51||!r((function(){var t=[];return(t.constructor={})[$e]=function(){return{foo:1}},1!==t[e](Boolean).foo}))},Je=Ve("isConcatSpreadable"),Qe=9007199254740991,Ze="Maximum allowed index exceeded",et=Ye>=51||!r((function(){var e=[];return e[Je]=!1,e.concat()[0]!==e})),tt=Ke("concat"),it=function(e){if(!y(e))return!1;var t=e[Je];return void 0!==t?!!t:Me(e)};Ne({target:"Array",proto:!0,forced:!et||!tt},{concat:function(e){var t,i,n,r,s,o=Pe(this),a=Xe(o,0),l=0;for(t=-1,n=arguments.length;t<n;t++)if(it(s=-1===t?o:arguments[t])){if(l+(r=ce(s.length))>Qe)throw TypeError(Ze);for(i=0;i<r;i++,l++)i in s&&De(a,l,s[i])}else{if(l>=Qe)throw TypeError(Ze);De(a,l++,s)}return a.length=l,a}});var nt=function(e,t,i){if(function(e){if("function"!=typeof e)throw TypeError(String(e)+" is not a function")}(e),void 0===t)return e;switch(i){case 0:return function(){return e.call(t)};case 1:return function(i){return e.call(t,i)};case 2:return function(i,n){return e.call(t,i,n)};case 3:return function(i,n,r){return e.call(t,i,n,r)}}return function(){return e.apply(t,arguments)}},rt=[].push,st=function(e){var t=1==e,i=2==e,n=3==e,r=4==e,s=6==e,o=5==e||s;return function(a,l,u,c){for(var d,h,p=Pe(a),v=f(p),y=nt(l,u,3),b=ce(v.length),m=0,g=c||Xe,_=t?g(a,b):i?g(a,0):void 0;b>m;m++)if((o||m in v)&&(h=y(d=v[m],m,p),e))if(t)_[m]=h;else if(h)switch(e){case 3:return!0;case 5:return d;case 6:return m;case 2:rt.call(_,d)}else if(r)return!1;return s?-1:n||r?r:_}},ot={forEach:st(0),map:st(1),filter:st(2),some:st(3),every:st(4),find:st(5),findIndex:st(6)},at=function(e,t){var i=[][e];return!!i&&r((function(){i.call(null,t||function(){throw 1},1)}))},lt=Object.defineProperty,ut={},ct=function(e){throw e},dt=function(e,t){if(g(ut,e))return ut[e];t||(t={});var i=[][e],n=!!g(t,"ACCESSORS")&&t.ACCESSORS,o=g(t,0)?t[0]:ct,a=g(t,1)?t[1]:void 0;return ut[e]=!!i&&!r((function(){if(n&&!s)return!0;var e={length:-1};n?lt(e,1,{enumerable:!0,get:ct}):e[1]=1,i.call(e,o,a)}))},ht=ot.forEach,ft=at("forEach"),pt=dt("forEach"),vt=ft&&pt?[].forEach:function(e){return ht(this,e,arguments.length>1?arguments[1]:void 0)};Ne({target:"Array",proto:!0,forced:[].forEach!=vt},{forEach:vt});var yt=ve.indexOf,bt=[].indexOf,mt=!!bt&&1/[1].indexOf(1,-0)<0,gt=at("indexOf"),_t=dt("indexOf",{ACCESSORS:!0,1:0});Ne({target:"Array",proto:!0,forced:mt||!gt||!_t},{indexOf:function(e){return mt?bt.apply(this,arguments)||0:yt(this,e,arguments.length>1?arguments[1]:void 0)}});var St=ot.map,wt=Ke("map"),Et=dt("map");Ne({target:"Array",proto:!0,forced:!wt||!Et},{map:function(e){return St(this,e,arguments.length>1?arguments[1]:void 0)}});var At,Lt=Object.setPrototypeOf||("__proto__"in{}?function(){var e,t=!1,i={};try{(e=Object.getOwnPropertyDescriptor(Object.prototype,"__proto__").set).call(i,[]),t=i instanceof Array}catch(e){}return function(i,n){return O(i),function(e){if(!y(e)&&null!==e)throw TypeError("Can't set "+String(e)+" as a prototype")}(n),t?e.call(i,n):i.__proto__=n,i}}():void 0),Ot=function(e,t,i){var n,r;return Lt&&"function"==typeof(n=t.constructor)&&n!==i&&y(r=n.prototype)&&r!==i.prototype&&Lt(e,r),e},Ct=Object.keys||function(e){return be(e,me)},kt=s?Object.defineProperties:function(e,t){O(e);for(var i,n=Ct(t),r=n.length,s=0;r>s;)k.f(e,i=n[s++],t[i]);return e},xt=se("document","documentElement"),jt=q("IE_PROTO"),Tt=function(){},Nt=function(e){return"<script>"+e+"</"+"script>"},Mt=function(){try{At=document.domain&&new ActiveXObject("htmlfile")}catch(e){}var e,t;Mt=At?function(e){e.write(Nt("")),e.close();var t=e.parentWindow.Object;return e=null,t}(At):((t=w("iframe")).style.display="none",xt.appendChild(t),t.src=String("javascript:"),(e=t.contentWindow.document).open(),e.write(Nt("document.F=Object")),e.close(),e.F);for(var i=me.length;i--;)delete Mt.prototype[me[i]];return Mt()};G[jt]=!0;var Pt=Object.create||function(e,t){var i;return null!==e?(Tt.prototype=O(e),i=new Tt,Tt.prototype=null,i[jt]=e):i=Mt(),void 0===t?i:kt(i,t)},Dt="\t\n\v\f\r                　\u2028\u2029\ufeff",It="["+Dt+"]",Ht=RegExp("^"+It+It+"*"),Bt=RegExp(It+It+"*$"),Ft=function(e){return function(t){var i=String(p(t));return 1&e&&(i=i.replace(Ht,"")),2&e&&(i=i.replace(Bt,"")),i}},Rt={start:Ft(1),end:Ft(2),trim:Ft(3)},Vt=_e.f,Wt=L.f,Xt=k.f,zt=Rt.trim,qt="Number",Gt=n.Number,Ut=Gt.prototype,Yt=d(Pt(Ut))==qt,$t=function(e){var t,i,n,r,s,o,a,l,u=b(e,!1);if("string"==typeof u&&u.length>2)if(43===(t=(u=zt(u)).charCodeAt(0))||45===t){if(88===(i=u.charCodeAt(2))||120===i)return NaN}else if(48===t){switch(u.charCodeAt(1)){case 66:case 98:n=2,r=49;break;case 79:case 111:n=8,r=55;break;default:return+u}for(o=(s=u.slice(2)).length,a=0;a<o;a++)if((l=s.charCodeAt(a))<48||l>r)return NaN;return parseInt(s,n)}return+u};if(je(qt,!Gt(" 0o1")||!Gt("0b1")||Gt("+0x1"))){for(var Kt,Jt=function(e){var t=arguments.length<1?0:e,i=this;return i instanceof Jt&&(Yt?r((function(){Ut.valueOf.call(i)})):d(i)!=qt)?Ot(new Gt($t(t)),i,Jt):$t(t)},Qt=s?Vt(Gt):"MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","),Zt=0;Qt.length>Zt;Zt++)g(Gt,Kt=Qt[Zt])&&!g(Jt,Kt)&&Xt(Jt,Kt,Wt(Gt,Kt));Jt.prototype=Ut,Ut.constructor=Jt,ie(n,qt,Jt)}var ei=Rt.trim,ti=n.parseInt,ii=/^[+-]?0[Xx]/,ni=8!==ti(Dt+"08")||22!==ti(Dt+"0x16")?function(e,t){var i=ei(String(e));return ti(i,t>>>0||(ii.test(i)?16:10))}:ti;Ne({target:"Number",stat:!0,forced:Number.parseInt!=ni},{parseInt:ni});var ri=Object.assign,si=Object.defineProperty,oi=!ri||r((function(){if(s&&1!==ri({b:1},ri(si({},"a",{enumerable:!0,get:function(){si(this,"b",{value:3,enumerable:!1})}}),{b:2})).b)return!0;var e={},t={},i=Symbol(),n="abcdefghijklmnopqrst";return e[i]=7,n.split("").forEach((function(e){t[e]=e})),7!=ri({},e)[i]||Ct(ri({},t)).join("")!=n}))?function(e,t){for(var i=Pe(e),n=arguments.length,r=1,o=Se.f,a=l.f;n>r;)for(var u,c=f(arguments[r++]),d=o?Ct(c).concat(o(c)):Ct(c),h=d.length,p=0;h>p;)u=d[p++],s&&!a.call(c,u)||(i[u]=c[u]);return i}:ri;Ne({target:"Object",stat:!0,forced:Object.assign!==oi},{assign:oi});var ai=l.f,li=function(e){return function(t){for(var i,n=v(t),r=Ct(n),o=r.length,a=0,l=[];o>a;)i=r[a++],s&&!ai.call(n,i)||l.push(e?[i,n[i]]:n[i]);return l}},ui={entries:li(!0),values:li(!1)}.entries;Ne({target:"Object",stat:!0},{entries:function(e){return ui(e)}});for(var ci in{CSSRuleList:0,CSSStyleDeclaration:0,CSSValueList:0,ClientRectList:0,DOMRectList:0,DOMStringList:0,DOMTokenList:1,DataTransferItemList:0,FileList:0,HTMLAllCollection:0,HTMLCollection:0,HTMLFormElement:0,HTMLSelectElement:0,MediaList:0,MimeTypeArray:0,NamedNodeMap:0,NodeList:1,PaintRequestList:0,Plugin:0,PluginArray:0,SVGLengthList:0,SVGNumberList:0,SVGPathSegList:0,SVGPointList:0,SVGStringList:0,SVGTransformList:0,SourceBufferList:0,StyleSheetList:0,TextTrackCueList:0,TextTrackList:0,TouchList:0}){var di=n[ci],hi=di&&di.prototype;if(hi&&hi.forEach!==vt)try{x(hi,"forEach",vt)}catch(e){hi.forEach=vt}}function fi(e){return(fi="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e})(e)}function pi(e,t){for(var i=0;i<t.length;i++){var n=t[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}function vi(e,t,i){return t in e?Object.defineProperty(e,t,{value:i,enumerable:!0,configurable:!0,writable:!0}):e[t]=i,e}function yi(e,t){var i=Object.keys(e);if(Object.getOwnPropertySymbols){var n=Object.getOwnPropertySymbols(e);t&&(n=n.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),i.push.apply(i,n)}return i}function bi(e){for(var t=1;t<arguments.length;t++){var i=null!=arguments[t]?arguments[t]:{};t%2?yi(Object(i),!0).forEach((function(t){vi(e,t,i[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(i)):yi(Object(i)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(i,t))}))}return e}function mi(e,t){if(null==e)return{};var i,n,r=function(e,t){if(null==e)return{};var i,n,r={},s=Object.keys(e);for(n=0;n<s.length;n++)i=s[n],t.indexOf(i)>=0||(r[i]=e[i]);return r}(e,t);if(Object.getOwnPropertySymbols){var s=Object.getOwnPropertySymbols(e);for(n=0;n<s.length;n++)i=s[n],t.indexOf(i)>=0||Object.prototype.propertyIsEnumerable.call(e,i)&&(r[i]=e[i])}return r}function gi(e,t){return function(e){if(Array.isArray(e))return e}(e)||function(e,t){if("undefined"==typeof Symbol||!(Symbol.iterator in Object(e)))return;var i=[],n=!0,r=!1,s=void 0;try{for(var o,a=e[Symbol.iterator]();!(n=(o=a.next()).done)&&(i.push(o.value),!t||i.length!==t);n=!0);}catch(e){r=!0,s=e}finally{try{n||null==a.return||a.return()}finally{if(r)throw s}}return i}(e,t)||function(e,t){if(!e)return;if("string"==typeof e)return _i(e,t);var i=Object.prototype.toString.call(e).slice(8,-1);"Object"===i&&e.constructor&&(i=e.constructor.name);if("Map"===i||"Set"===i)return Array.from(e);if("Arguments"===i||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i))return _i(e,t)}(e,t)||function(){throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function _i(e,t){(null==t||t>e.length)&&(t=e.length);for(var i=0,n=new Array(t);i<t;i++)n[i]=e[i];return n}var Si=function(e,t,i){var n;return void 0===t&&(t=50),void 0===i&&(i={isImmediate:!1}),function(){for(var r=[],s=arguments.length;s--;)r[s]=arguments[s];var o=this,a=i.isImmediate&&void 0===n;void 0!==n&&clearTimeout(n),n=setTimeout((function(){n=void 0,i.isImmediate||e.apply(o,r)}),t),a&&e.apply(o,r)}},wi=Ke("slice"),Ei=dt("slice",{ACCESSORS:!0,0:0,1:2}),Ai=Ve("species"),Li=[].slice,Oi=Math.max;Ne({target:"Array",proto:!0,forced:!wi||!Ei},{slice:function(e,t){var i,n,r,s=v(this),o=ce(s.length),a=fe(e,o),l=fe(void 0===t?o:t,o);if(Me(s)&&("function"!=typeof(i=s.constructor)||i!==Array&&!Me(i.prototype)?y(i)&&null===(i=i[Ai])&&(i=void 0):i=void 0,i===Array||void 0===i))return Li.call(s,a,l);for(n=new(void 0===i?Array:i)(Oi(l-a,0)),r=0;a<l;a++,r++)a in s&&De(n,r,s[a]);return n.length=r,n}});var Ci={};Ci[Ve("toStringTag")]="z";var ki="[object z]"===String(Ci),xi=Ve("toStringTag"),ji="Arguments"==d(function(){return arguments}()),Ti=ki?d:function(e){var t,i,n;return void 0===e?"Undefined":null===e?"Null":"string"==typeof(i=function(e,t){try{return e[t]}catch(e){}}(t=Object(e),xi))?i:ji?d(t):"Object"==(n=d(t))&&"function"==typeof t.callee?"Arguments":n},Ni=ki?{}.toString:function(){return"[object "+Ti(this)+"]"};ki||ie(Object.prototype,"toString",Ni,{unsafe:!0});var Mi=function(){var e=O(this),t="";return e.global&&(t+="g"),e.ignoreCase&&(t+="i"),e.multiline&&(t+="m"),e.dotAll&&(t+="s"),e.unicode&&(t+="u"),e.sticky&&(t+="y"),t},Pi="toString",Di=RegExp.prototype,Ii=Di.toString,Hi=r((function(){return"/a/b"!=Ii.call({source:"a",flags:"b"})})),Bi=Ii.name!=Pi;(Hi||Bi)&&ie(RegExp.prototype,Pi,(function(){var e=O(this),t=String(e.source),i=e.flags;return"/"+t+"/"+String(void 0===i&&e instanceof RegExp&&!("flags"in Di)?Mi.call(e):i)}),{unsafe:!0});var Fi,Ri,Vi,Wi,Xi=function(e){var t=(new DOMParser).parseFromString(e,"text/html").body.firstChild;if(t instanceof HTMLElement)return t;throw new Error("Supplied markup does not create an HTML Element")},zi=function(e){var t=e.charCode||e.keyCode,i=e.type;return"click"===i||"keydown"===i&&(32===t||13===t)&&(e.preventDefault(),!0)},qi=function(e){return"number"==typeof e&&isFinite(e)&&Math.floor(e)===e&&null!=e&&!isNaN(Number(e.toString()))},Gi=function(e){return"object"===fi(e)&&null!==e},Ui=function(e,t){if(void 0!==e){var i=function(e){if(Array.isArray(e))return!0;var t=Object.prototype.toString.call(e);return!("[object HTMLCollection]"!==t&&"[object NodeList]"!==t&&("object"!==fi(e)||!e.hasOwnProperty("length")||e.length<0||0!==e.length&&(!e[0]||!e[0].nodeType)))}(e)?e:[e];Array.prototype.slice.call(i).forEach((function(e){e instanceof HTMLElement&&t&&t(e)}))}},Yi=function(e,t,i){var n=!!window.getComputedStyle,r=window.getComputedStyle||function(){},s=function(){if(!n)return!1;var e=document.body||document.documentElement,t=document.createElement("div");t.style.cssText="width:10px;padding:2px;-webkit-box-sizing:border-box;box-sizing:border-box;",e.appendChild(t);var i="10px"===r(t,null).width;return e.removeChild(t),i}(),o=function(e){return e=parseFloat(e),t=e,!isNaN(parseFloat(t))&&isFinite(t)?e:0;var t};return function(e,t,i){if(n){i=i||r(e,null);var a=o(i[t]);return s||"width"!==t?s||"height"!==t||(a+=o(i.paddingTop)+o(i.paddingBottom)+o(i.borderTopWidth)+o(i.borderBottomWidth)):a+=o(i.paddingLeft)+o(i.paddingRight)+o(i.borderLeftWidth)+o(i.borderRightWidth),a}return o(e.style[t])}(e,t,i)};return function(e){e[e.Prev=0]="Prev",e[e.Next=1]="Next"}(Fi||(Fi={})),function(e){e[e.Enabled=1]="Enabled",e[e.Disabled=0]="Disabled"}(Ri||(Ri={})),function(e){e[e.Enable=0]="Enable",e[e.Disable=1]="Disable"}(Vi||(Vi={})),function(e){e[e.Yes=0]="Yes",e[e.No=0]="No"}(Wi||(Wi={})),function(){function e(t,i){var n=this;if(function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,e),!(t instanceof HTMLElement))throw new Error("The required input [element] must be an HTMLElement");if(void 0!==i&&!Gi(i))throw new Error("The required input [options] must be an Object");this.slider=t,this.slides=t.children,this.sliderContainer=Xi('<div class="a11y-slider-container"></div>'),this._activeClass="a11y-slider-active",this._visibleClass="a11y-slider-visible",this._dotsClass="a11y-slider-dots",this._sliderClass="a11y-slider",this._focusable="a, area, input, select, textarea, button, iframe, object, embed, *[tabindex], *[contenteditable]",this._autoplayTimer=Wi.No,this.autoplayBtn=Xi('<button type="button" class="a11y-slider-autoplay">Toggle slider autoplay</button>'),this._pauseOnMouseLeave=!1,this._skipBtns=[],this.dots=null,this.swipe=!0,this.activeSlide=this.slides[0],this.visibleSlides=[],this.sliderEnabled=Ri.Disabled,this.modernBrowser=!!HTMLElement.prototype.scrollTo,this.mouseDown=!1,this.swipeStartX=0,this.swipeX=0,this.swipeXCached=0,this._hasCustomArrows=!!(i&&i.prevArrow||i&&i.nextArrow),this.options={container:!0,arrows:!0,prevArrow:i&&i.prevArrow||Xi('<button type="button" class="a11y-slider-prev">Previous slide</button>'),nextArrow:i&&i.nextArrow||Xi('<button type="button" class="a11y-slider-next">Next slide</button>'),dots:!0,adaptiveHeight:!1,skipBtn:!0,slidesToShow:null,autoplay:!1,autoplaySpeed:4e3,autoplayHoverPause:!0,centerMode:!1,infinite:!0,disable:!1,responsive:null,customPaging:null,swipe:!0},this.options=bi(bi({},this.options),i),this._handlePrev=this._handlePrev.bind(this),this._handleNext=this._handleNext.bind(this),this._handleAutoplay=this._handleAutoplay.bind(this),this._handleAutoplayHover=this._handleAutoplayHover.bind(this),this._checkShouldEnableDebounced=Si(this._checkShouldEnable.bind(this),250),this._updateHeightDebounced=Si(this._updateHeight.bind(this),250),this._generateDotsDebounced=Si(this._generateDots.bind(this),250),this._updateScrollPosition=Si((function(){return n.scrollToSlide(n.activeSlide)}),250),this._handleScroll=Si(this._handleScroll.bind(this),10),this._scrollFinish=Si(this._scrollFinish.bind(this),175),this._swipeMouseDown=this._swipeMouseDown.bind(this),this._swipeMouseUp=this._swipeMouseUp.bind(this),this._swipeMouseMove=this._swipeMouseMove.bind(this),this._init()}var t,i,n;return t=e,(i=[{key:"_init",value:function(){var e=this;setTimeout((function(){return e.slider.scrollLeft=0}),1),Gi(this.options.responsive)&&this._checkResponsive(),this._checkShouldEnable(),window.addEventListener("resize",this._checkShouldEnableDebounced),this._dispatchEvent("init",{a11ySlider:this})}},{key:"_checkShouldEnable",value:function(){var e=this,t=!0;!0===this.options.disable&&(t=!1),this.slides.length<=1&&(t=!1),qi(this.options.slidesToShow)?this.slides.length===this.options.slidesToShow&&(t=!1):this._getActiveAndVisible(null,(function(i){i.length===e.slides.length&&(t=!1)})),t&&this.sliderEnabled===Ri.Disabled?this._enableSlider():t||this.sliderEnabled!==Ri.Enabled||this._disableSlider(),!t&&this._hasCustomArrows&&(Ui(this.options.prevArrow,(function(e){e.classList.add("a11y-slider-hide")})),Ui(this.options.nextArrow,(function(e){e.classList.add("a11y-slider-hide")})))}},{key:"_enableSlider",value:function(){var e=this;this.sliderEnabled=Ri.Enabled,this.options.container&&(this.slider.insertAdjacentElement("beforebegin",this.sliderContainer),this.sliderContainer.insertAdjacentElement("afterbegin",this.slider)),this.options.skipBtn&&this._addSkipBtn(),this.options.arrows&&!this._hasCustomArrows&&(this.options.prevArrow instanceof HTMLElement&&this.slider.insertAdjacentElement("beforebegin",this.options.prevArrow),this.options.nextArrow instanceof HTMLElement&&this.slider.insertAdjacentElement("beforebegin",this.options.nextArrow)),Ui(this.options.prevArrow,(function(t){t.addEventListener("click",e._handlePrev,{passive:!0}),t.addEventListener("keypress",e._handlePrev,{passive:!0}),e._hasCustomArrows&&t.classList.remove("a11y-slider-hide")})),Ui(this.options.nextArrow,(function(t){t.addEventListener("click",e._handleNext,{passive:!0}),t.addEventListener("keypress",e._handleNext,{passive:!0}),e._hasCustomArrows&&t.classList.remove("a11y-slider-hide")})),this.options.dots&&this._generateDots(),this.slider.addEventListener("scroll",this._handleScroll,!1),this._setCSS(),!0===this.options.adaptiveHeight&&(this._updateHeight(this.activeSlide),window.addEventListener("resize",this._updateHeightDebounced.bind(this))),this.options.autoplay&&this._enableAutoplay(),window.addEventListener("resize",this._updateScrollPosition),this.options.swipe&&this._enableSwipe()}},{key:"_disableSlider",value:function(){var e=this;this.sliderEnabled=Ri.Disabled,document.body.contains(this.sliderContainer)&&(this.sliderContainer.insertAdjacentElement("beforebegin",this.slider),this.sliderContainer.parentNode&&this.sliderContainer.parentNode.removeChild(this.sliderContainer)),this._removeSkipBtn(),Ui(this.options.prevArrow,(function(t){t.removeEventListener("click",e._handlePrev),t.removeEventListener("keypress",e._handlePrev),e._hasCustomArrows?t.classList.add("a11y-slider-hide"):t.parentNode&&t.parentNode.removeChild(t)})),Ui(this.options.nextArrow,(function(t){t.removeEventListener("click",e._handleNext),t.removeEventListener("keypress",e._handleNext),e._hasCustomArrows?t.classList.add("a11y-slider-hide"):t.parentNode&&t.parentNode.removeChild(t)})),this._removeDots(),this.slider.removeEventListener("scroll",this._handleScroll,!1),this._removeCSS(),window.removeEventListener("resize",this._updateHeightDebounced),this._updateHeight(!1),this.options.autoplay&&this._disableAutoplay(),this._disableSwipe(),window.removeEventListener("resize",this._updateScrollPosition),this.options.swipe&&this._disableSwipe()}},{key:"_setCSS",value:function(e){var t=this;this._addSlidesWidth(),this._getActiveAndVisible(e||null),this.slider.classList.add(this._sliderClass),Ui(this.slides,(function(e){e.classList.remove(t._activeClass),e.classList.remove(t._visibleClass)})),this.activeSlide.classList.add(this._activeClass),Ui(this.visibleSlides,(function(e){e.classList.add(t._visibleClass)})),this._updateDots(this.activeSlide),this._updateA11Y()}},{key:"_removeCSS",value:function(){var e=this;this._removeSlidesWidth(),this.slider.classList.remove(this._sliderClass),Ui(this.slides,(function(t){t.classList.remove(e._activeClass),t.classList.remove(e._visibleClass)})),this._removeA11Y()}},{key:"_checkResponsive",value:function(){var e=this;if(Gi(this.options.responsive)){var t=this.options,i=(t.responsive,mi(t,["responsive"])),n=[],r=Object.entries(this.options.responsive).sort((function(e,t){return e[1]-t[1]}));n.push({mql:window.matchMedia("screen and (max-width: ".concat(Number.parseInt(r[0][0])-1,"px)")),options:i}),r.forEach((function(t,i){var s=gi(t,2),o=s[0],a=s[1],l=bi({},e.options),u="screen and (min-width: ".concat(o,"px)");i!==r.length-1&&(u=u.concat(" and (max-width: ".concat(Number.parseInt(r[i+1][0])-1,"px)"))),n.forEach((function(e){Object.assign(l,e.options)})),Object.assign(l,a),n.push({mql:window.matchMedia(u),options:l})})),n.map((function(t){t.mql.matches&&Object.assign(e.options,t.options),t.mql.addListener((function(){t.mql.matches&&e.updateOptions(t.options)}))}))}}},{key:"_addSlidesWidth",value:function(){if(qi(this.options.slidesToShow)){var e=100/this.options.slidesToShow;this.slider.style.display="flex",Ui(this.slides,(function(t){t.style.width="".concat(e,"%"),t.style.flex="0 0 auto"}))}else this._removeSlidesWidth()}},{key:"_removeSlidesWidth",value:function(){this.slider.style.removeProperty("display"),Ui(this.slides,(function(e){e.style.removeProperty("width"),e.style.removeProperty("flex")}))}},{key:"_updateA11Y",value:function(){var e=this;if(this._removeA11Y(),Ui(this.slides,(function(t){var i=t.querySelectorAll(e._focusable);t.classList.contains(e._visibleClass)||(t.setAttribute("tabindex","-1"),t.setAttribute("aria-hidden","true")),Ui(i,(function(i){t.classList.contains(e._visibleClass)||i.setAttribute("tabindex","-1")}))})),!1===this.options.infinite){var t=this.slider.firstElementChild,i=this.slider.lastElementChild,n=this.visibleSlides[0],r=this.visibleSlides[this.visibleSlides.length-1];n===t&&Ui(this.options.prevArrow,(function(e){e.setAttribute("disabled","")})),r===i&&Ui(this.options.nextArrow,(function(e){e.setAttribute("disabled","")}))}}},{key:"_removeA11Y",value:function(){var e=this;Ui(this.slides,(function(t){var i=t.querySelectorAll(e._focusable);t.removeAttribute("tabindex"),t.removeAttribute("aria-hidden"),Ui(i,(function(e){e.removeAttribute("tabindex")}))})),Ui(this.options.prevArrow,(function(e){return e.removeAttribute("disabled")})),Ui(this.options.nextArrow,(function(e){return e.removeAttribute("disabled")}))}},{key:"_addSkipBtn",value:function(){var e=Xi('<button class="a11y-slider-sr-only" type="button" tabindex="0">Click to skip slider carousel</button>'),t=Xi('<div class="a11y-slider-sr-only" tabindex="-1">End of slider carousel</div>'),i=function(e){!0===zi(e)&&t.focus()};e.addEventListener("click",i,{passive:!0}),e.addEventListener("keypress",i,{passive:!0}),this.slider.insertAdjacentElement("beforebegin",e),this.slider.insertAdjacentElement("afterend",t),this._skipBtns=[],this._skipBtns.push(e,t)}},{key:"_removeSkipBtn",value:function(){Ui(this._skipBtns,(function(e){e.parentNode&&e.parentNode.removeChild(e)}))}},{key:"_generateDots",value:function(){var e=this;if(!1!==this.options.dots&&(this._removeDots(),this.sliderEnabled!==Ri.Disabled)){this.dots=Xi('<ul class="'.concat(this._dotsClass,'"></ul>'));for(var t=function(t){var i=Xi("<li></li>"),n=void 0;e.options.customPaging?n=Xi(e.options.customPaging(t,e)):(n=Xi('<button type="button"></button>')).textContent="Move slider to item #".concat(t+1);var r=function(i){!0===zi(i)&&(e.scrollToSlide(e.slides[t]),e._toggleAutoplay(Vi.Disable))};n.addEventListener("click",r,{passive:!0}),n.addEventListener("keypress",r,{passive:!0}),i.insertAdjacentElement("beforeend",n),e.dots.insertAdjacentElement("beforeend",i)},i=0;i<this._getDotCount();i++)t(i);this._updateDots(this.activeSlide),this.slider.insertAdjacentElement("afterend",this.dots),window.addEventListener("resize",this._generateDotsDebounced)}}},{key:"_getDotCount",value:function(){return this.slides.length-(this.options.slidesToShow||this.visibleSlides.length)+1}},{key:"_removeDots",value:function(){window.removeEventListener("resize",this._generateDotsDebounced),this.dots instanceof HTMLElement&&this.dots.parentNode&&this.dots.parentNode.removeChild(this.dots)}},{key:"_updateDots",value:function(e){if(this.dots instanceof HTMLElement){var t,i=Array.prototype.indexOf.call(e.parentNode&&e.parentNode.children,e);i>this.dots.children.length&&(i=this.dots.children.length-1),Ui(this.dots.children,(function(e){var t;return null===(t=e.querySelector("button"))||void 0===t?void 0:t.classList.remove("active")})),null===(t=this.dots.children[i].querySelector("button"))||void 0===t||t.classList.add("active")}}},{key:"_enableAutoplay",value:function(){this.autoplayBtn.addEventListener("click",this._handleAutoplay,{passive:!0}),this.autoplayBtn.addEventListener("keypress",this._handleAutoplay,{passive:!0}),this.options.autoplayHoverPause&&(this.slider.addEventListener("mouseenter",this._handleAutoplayHover,{passive:!0}),this.slider.addEventListener("mouseleave",this._handleAutoplayHover,{passive:!0})),this.slider.insertAdjacentElement("beforebegin",this.autoplayBtn),this._toggleAutoplay(Vi.Enable)}},{key:"_disableAutoplay",value:function(){var e;this._toggleAutoplay(Vi.Disable),this.autoplayBtn.removeEventListener("click",this._handleAutoplay),this.autoplayBtn.removeEventListener("keypress",this._handleAutoplay),this.slider.removeEventListener("mouseenter",this._handleAutoplayHover),this.slider.removeEventListener("mouseleave",this._handleAutoplayHover),null===(e=this.autoplayBtn.parentNode)||void 0===e||e.removeChild(this.autoplayBtn)}},{key:"_enableSwipe",value:function(){this.options.swipe&&(this.slider.addEventListener("mousedown",this._swipeMouseDown),this.slider.addEventListener("mouseleave",this._swipeMouseUp),this.slider.addEventListener("mouseup",this._swipeMouseUp),this.slider.addEventListener("mousemove",this._swipeMouseMove))}},{key:"_swipeMouseDown",value:function(e){this.mouseDown=!0,this.slider.classList.add("a11y-slider-scrolling"),this.swipeStartX=e.pageX-this.slider.offsetLeft,this.swipeX=this.slider.scrollLeft,this.swipeXCached=this.slider.scrollLeft}},{key:"_swipeMouseUp",value:function(){this.mouseDown&&(this.mouseDown=!1,this.slider.classList.remove("a11y-slider-scrolling"),this.modernBrowser&&this.slider.scroll({left:this.swipeXCached-1,behavior:"smooth"}))}},{key:"_swipeMouseMove",value:function(e){if(this.mouseDown){e.preventDefault();var t=2*(e.pageX-this.slider.offsetLeft-this.swipeStartX);this.slider.scrollLeft=this.swipeX-t,this.swipeXCached=this.slider.scrollLeft}}},{key:"_disableSwipe",value:function(){this.slider.removeEventListener("mousedown",this._swipeMouseDown),this.slider.removeEventListener("mouseleave",this._swipeMouseUp),this.slider.removeEventListener("mouseup",this._swipeMouseUp),this.slider.removeEventListener("mousemove",this._swipeMouseMove)}},{key:"_toggleAutoplay",value:function(e){var t=this;e===Vi.Enable?(t._autoplayTimer=window.setInterval((function(){t._goPrevOrNext(Fi.Next)}),t.options.autoplaySpeed),t.autoplayBtn.setAttribute("data-autoplaying","true"),t._dispatchEvent("autoplayStart",{currentSlide:t.activeSlide,a11ySlider:t})):e===Vi.Disable&&(window.clearInterval(t._autoplayTimer),t._autoplayTimer=Wi.No,t.autoplayBtn.setAttribute("data-autoplaying","false"),t._dispatchEvent("autoplayStop",{currentSlide:t.activeSlide,a11ySlider:t}))}},{key:"_goPrevOrNext",value:function(e){var t=this;this._getActiveAndVisible(null,(function(i,n){var r=t.slider.firstElementChild,s=t.slider.lastElementChild,o=i[0],a=i[i.length-1];e===Fi.Next?a===s?!0===t.options.infinite&&t.scrollToSlide(r):t.scrollToSlide(n&&n.nextElementSibling):e===Fi.Prev&&(o===r?!0===t.options.infinite&&t.scrollToSlide(s):t.scrollToSlide(n&&n.previousElementSibling))}))}},{key:"scrollToSlide",value:function(e){var t,i=this,n=this.slider.scrollLeft;if(qi(e))t=this.slides[e];else{if(!(e instanceof HTMLElement))throw new Error("scrollToSlide only accepts an HTMLElement or number");t=e}this._dispatchEvent("beforeChange",{currentSlide:this.activeSlide,nextSlide:t,a11ySlider:this}),!0===this.options.adaptiveHeight&&this._updateHeight(t),this.modernBrowser?this.slider.scroll({left:t.offsetLeft,behavior:"smooth"}):this.slider.scrollLeft=t.offsetLeft,setTimeout((function(){i.slider.scrollLeft===n&&i.sliderEnabled===Ri.Enabled&&i._setCSS(t)}),50),this._updateDots(t)}},{key:"updateOptions",value:function(e){Object.assign(this.options,e),this._disableSlider(),this._checkShouldEnable()}},{key:"_updateHeight",value:function(e){if(e instanceof HTMLElement){var t=Yi(e,"height");this.slider.style.height="".concat(t,"px")}else this.slider.style.height=""}},{key:"refreshHeight",value:function(){this._updateHeight(this.activeSlide)}},{key:"_getActiveAndVisible",value:function(e,t){var i=!this.slider.classList.contains(this._sliderClass),n=this.slider.style.borderWidth;this.slider.style.borderWidth="0px",i&&this.slider.classList.add(this._sliderClass);var r=[],s=Math.round(this.slider.getBoundingClientRect().width),o=this.slider.scrollLeft-1<0?0:this.slider.scrollLeft-1;if(Ui(this.slides,(function(e){var t=e.offsetLeft;t>=o&&t<o+s&&r.push(e)})),this.slider.style.borderWidth=n,i&&this.slider.classList.remove(this._sliderClass),this.visibleSlides=r,e)this.activeSlide=e;else if(!0===this.options.centerMode)this.activeSlide=this.visibleSlides[Math.floor((this.visibleSlides.length-1)/2)];else{var a;this.activeSlide=null!==(a=r[0])&&void 0!==a?a:this.slides[0]}t&&t(this.visibleSlides,this.activeSlide)}},{key:"_handlePrev",value:function(e){!0===zi(e)&&(this._goPrevOrNext(Fi.Prev),this._toggleAutoplay(Vi.Disable))}},{key:"_handleNext",value:function(e){!0===zi(e)&&(this._goPrevOrNext(Fi.Next),this._toggleAutoplay(Vi.Disable))}},{key:"_handleAutoplay",value:function(e){!0===zi(e)&&(this._autoplayTimer===Wi.No?this._toggleAutoplay(Vi.Enable):this._toggleAutoplay(Vi.Disable))}},{key:"_handleAutoplayHover",value:function(e){"mouseenter"===e.type?this._autoplayTimer!==Wi.No&&(this._toggleAutoplay(Vi.Disable),this._pauseOnMouseLeave=!0):"mouseleave"===e.type&&this._pauseOnMouseLeave&&this._autoplayTimer===Wi.No&&(this._toggleAutoplay(Vi.Enable),this._pauseOnMouseLeave=!1)}},{key:"_handleScroll",value:function(){this._scrollFinish()}},{key:"_scrollFinish",value:function(){this._setCSS(),this._dispatchEvent("afterChange",{currentSlide:this.activeSlide,a11ySlider:this})}},{key:"_dispatchEvent",value:function(e,t){var i=function(e,t){var i=document.createEvent("CustomEvent");return t=t||{bubbles:!1,cancelable:!1,detail:void 0},i.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),i}(e,{detail:t});this.slider.dispatchEvent(i)}},{key:"destroy",value:function(){this._disableSlider(),window.removeEventListener("resize",this._checkShouldEnableDebounced),this._dispatchEvent("destroy",{a11ySlider:this})}}])&&pi(t.prototype,i),n&&pi(t,n),e}()}));

// slick slider
!function(i){"use strict";"function"==typeof define&&define.amd?define(["jquery"],i):"undefined"!=typeof exports?module.exports=i(require("jquery")):i(jQuery)}(function(d){"use strict";var s,l=window.Slick||{};s=0,(l=function(i,e){var t,o=this;o.defaults={adaptiveHeight:!1,appendArrows:d(i),appendDots:d(i),arrows:!0,arrowsPlacement:null,asNavFor:null,prevArrow:'<button class="slick-prev" type="button"><span class="slick-prev-icon" aria-hidden="true"></span><span class="slick-sr-only">Previous</span></button>',nextArrow:'<button class="slick-next" type="button"><span class="slick-next-icon" aria-hidden="true"></span><span class="slick-sr-only">Next</span></button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(i,e){return d('<button type="button"><span class="slick-dot-icon" aria-hidden="true"></span><span class="slick-sr-only">Go to slide '+(e+1)+"</span></button>")},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,infinite:!0,initialSlide:0,instructionsText:null,lazyLoad:"ondemand",mobileFirst:!1,playIcon:'<span class="slick-play-icon" aria-hidden="true"></span>',pauseIcon:'<span class="slick-pause-icon" aria-hidden="true"></span>',pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,regionLabel:"carousel",respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useAutoplayToggleButton:!0,useCSS:!0,useGroupRole:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},o.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,$instructionsText:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$pauseButton:null,$pauseIcon:null,$playIcon:null,$prevArrow:null,scrolling:!1,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,swiping:!1,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},d.extend(o,o.initials),o.activeBreakpoint=null,o.animType=null,o.animProp=null,o.breakpoints=[],o.breakpointSettings=[],o.cssTransitions=!1,o.focussed=!1,o.interrupted=!1,o.hidden="hidden",o.paused=!0,o.positionProp=null,o.respondTo=null,o.rowCount=1,o.shouldClick=!0,o.$slider=d(i),o.$slidesCache=null,o.transformType=null,o.transitionType=null,o.visibilityChange="visibilitychange",o.windowWidth=0,o.windowTimer=null,t=d(i).data("slick")||{},o.options=d.extend({},o.defaults,e,t),o.currentSlide=o.options.initialSlide,o.originalSettings=o.options,void 0!==document.mozHidden?(o.hidden="mozHidden",o.visibilityChange="mozvisibilitychange"):void 0!==document.webkitHidden&&(o.hidden="webkitHidden",o.visibilityChange="webkitvisibilitychange"),o.autoPlay=d.proxy(o.autoPlay,o),o.autoPlayClear=d.proxy(o.autoPlayClear,o),o.autoPlayIterator=d.proxy(o.autoPlayIterator,o),o.autoPlayToggleHandler=d.proxy(o.autoPlayToggleHandler,o),o.changeSlide=d.proxy(o.changeSlide,o),o.clickHandler=d.proxy(o.clickHandler,o),o.selectHandler=d.proxy(o.selectHandler,o),o.setPosition=d.proxy(o.setPosition,o),o.swipeHandler=d.proxy(o.swipeHandler,o),o.dragHandler=d.proxy(o.dragHandler,o),o.instanceUid=s++,o.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,o.registerBreakpoints(),o.init(!0)}).prototype.addSlide=l.prototype.slickAdd=function(i,e,t){var o=this;if("boolean"==typeof e)t=e,e=null;else if(e<0||e>=o.slideCount)return!1;o.unload(),"number"==typeof e?0===e&&0===o.$slides.length?d(i).appendTo(o.$slideTrack):t?d(i).insertBefore(o.$slides.eq(e)):d(i).insertAfter(o.$slides.eq(e)):!0===t?d(i).prependTo(o.$slideTrack):d(i).appendTo(o.$slideTrack),o.$slides=o.$slideTrack.children(this.options.slide),o.$slideTrack.children(this.options.slide).detach(),o.$slideTrack.append(o.$slides),o.$slides.each(function(i,e){d(e).attr("data-slick-index",i),d(e).attr("role","group"),d(e).attr("aria-label","slide "+i)}),o.$slidesCache=o.$slides,o.reinit()},l.prototype.animateHeight=function(){var i,e=this;1===e.options.slidesToShow&&!0===e.options.adaptiveHeight&&!1===e.options.vertical&&(i=e.$slides.eq(e.currentSlide).outerHeight(!0),e.$list.animate({height:i},e.options.speed))},l.prototype.animateSlide=function(i,e){var t={},o=this;o.animateHeight(),!0===o.options.rtl&&!1===o.options.vertical&&(i=-i),!1===o.transformsEnabled?!1===o.options.vertical?o.$slideTrack.animate({left:i},o.options.speed,o.options.easing,e):o.$slideTrack.animate({top:i},o.options.speed,o.options.easing,e):!1===o.cssTransitions?(!0===o.options.rtl&&(o.currentLeft=-o.currentLeft),d({animStart:o.currentLeft}).animate({animStart:i},{duration:o.options.speed,easing:o.options.easing,step:function(i){i=Math.ceil(i),!1===o.options.vertical?t[o.animType]="translate("+i+"px, 0px)":t[o.animType]="translate(0px,"+i+"px)",o.$slideTrack.css(t)},complete:function(){e&&e.call()}})):(o.applyTransition(),i=Math.ceil(i),!1===o.options.vertical?t[o.animType]="translate3d("+i+"px, 0px, 0px)":t[o.animType]="translate3d(0px,"+i+"px, 0px)",o.$slideTrack.css(t),e&&setTimeout(function(){o.disableTransition(),e.call()},o.options.speed))},l.prototype.getNavTarget=function(){var i=this.options.asNavFor;return i&&null!==i&&(i=d(i).not(this.$slider)),i},l.prototype.asNavFor=function(e){var i=this.getNavTarget();null!==i&&"object"==typeof i&&i.each(function(){var i=d(this).slick("getSlick");i.unslicked||i.slideHandler(e,!0)})},l.prototype.applyTransition=function(i){var e=this,t={};!1===e.options.fade?t[e.transitionType]=e.transformType+" "+e.options.speed+"ms "+e.options.cssEase:t[e.transitionType]="opacity "+e.options.speed+"ms "+e.options.cssEase,!1===e.options.fade?e.$slideTrack.css(t):e.$slides.eq(i).css(t)},l.prototype.autoPlay=function(){var i=this;i.autoPlayClear(),i.slideCount>i.options.slidesToShow&&(i.autoPlayTimer=setInterval(i.autoPlayIterator,i.options.autoplaySpeed))},l.prototype.autoPlayClear=function(){this.autoPlayTimer&&clearInterval(this.autoPlayTimer)},l.prototype.autoPlayIterator=function(){var i=this,e=i.currentSlide+i.options.slidesToScroll;i.paused||i.interrupted||i.focussed||(!1===i.options.infinite&&(1===i.direction&&i.currentSlide+1===i.slideCount-1?i.direction=0:0===i.direction&&(e=i.currentSlide-i.options.slidesToScroll,i.currentSlide-1==0&&(i.direction=1))),i.slideHandler(e))},l.prototype.autoPlayToggleHandler=function(){var i=this;i.paused?(i.$playIcon.css("display","none"),i.$pauseIcon.css("display","inline"),i.$pauseButton.find(".slick-play-text").attr("style","display: none"),i.$pauseButton.find(".slick-pause-text").removeAttr("style"),i.slickPlay()):(i.$playIcon.css("display","inline"),i.$pauseIcon.css("display","none"),i.$pauseButton.find(".slick-play-text").removeAttr("style"),i.$pauseButton.find(".slick-pause-text").attr("style","display: none"),i.slickPause())},l.prototype.buildArrows=function(){var i=this;if(!0===i.options.arrows)if(i.$prevArrow=d(i.options.prevArrow).addClass("slick-arrow"),i.$nextArrow=d(i.options.nextArrow).addClass("slick-arrow"),i.slideCount>i.options.slidesToShow){if(i.htmlExpr.test(i.options.prevArrow))if(null!=i.options.arrowsPlacement)switch(i.options.arrowsPlacement){case"beforeSlides":case"split":console.log("test"),i.$prevArrow.prependTo(i.options.appendArrows);break;case"afterSlides":i.$prevArrow.appendTo(i.options.appendArrows)}else i.$prevArrow.prependTo(i.options.appendArrows);if(i.htmlExpr.test(i.options.nextArrow))if(null!=i.options.arrowsPlacement)switch(i.options.arrowsPlacement){case"beforeSlides":console.log("test2"),i.$prevArrow.after(i.$nextArrow);break;case"afterSlides":case"split":i.$nextArrow.appendTo(i.options.appendArrows)}else i.$nextArrow.appendTo(i.options.appendArrows);!0!==i.options.infinite&&i.$prevArrow.addClass("slick-disabled").prop("disabled",!0)}else i.$prevArrow.add(i.$nextArrow).addClass("slick-hidden").prop("disabled",!0)},l.prototype.buildDots=function(){var i,e,t=this;if(!0===t.options.dots&&t.slideCount>t.options.slidesToShow){for(t.$slider.addClass("slick-dotted"),e=d("<ul />").addClass(t.options.dotsClass),i=0;i<=t.getDotCount();i+=1)e.append(d("<li />").append(t.options.customPaging.call(this,t,i)));t.$dots=e.appendTo(t.options.appendDots),t.$dots.find("li").first().addClass("slick-active")}},l.prototype.buildOut=function(){var t=this;t.$slides=t.$slider.children(t.options.slide+":not(.slick-cloned)").addClass("slick-slide"),t.slideCount=t.$slides.length,t.$slides.each(function(i,e){d(e).attr("data-slick-index",i).data("originalStyling",d(e).attr("style")||""),t.options.useGroupRole&&d(e).attr("role","group").attr("aria-label","slide "+(i+1))}),t.$slider.addClass("slick-slider"),t.$slider.attr("role","region"),t.$slider.attr("aria-label",t.options.regionLabel),t.$slideTrack=0===t.slideCount?d('<div class="slick-track"/>').appendTo(t.$slider):t.$slides.wrapAll('<div class="slick-track"/>').parent(),t.$list=t.$slideTrack.wrap('<div class="slick-list"/>').parent(),t.$slideTrack.css("opacity",0),!0!==t.options.centerMode&&!0!==t.options.swipeToSlide||(t.options.slidesToScroll=1),d("img[data-lazy]",t.$slider).not("[src]").addClass("slick-loading"),t.setupInfinite(),t.buildArrows(),t.buildDots(),t.updateDots(),t.setSlideClasses("number"==typeof t.currentSlide?t.currentSlide:0),!0===t.options.draggable&&t.$list.addClass("draggable"),t.options.autoplay&&t.options.useAutoplayToggleButton&&(t.$pauseIcon=d(t.options.pauseIcon).attr("aria-hidden",!0),t.$playIcon=d(t.options.playIcon).attr("aria-hidden",!0),t.$pauseButton=d('<button type="button" class="slick-autoplay-toggle-button">'),t.$pauseButton.append(t.$pauseIcon),t.$pauseButton.append(t.$playIcon.css("display","none")),t.$pauseButton.append(d('<span class="slick-pause-text slick-sr-only">Pause</span>')),t.$pauseButton.append(d('<span class="slick-play-text slick-sr-only" style="display: none">Play</span>')),t.$pauseButton.prependTo(t.$slider)),null!=t.options.instructionsText&&""!=t.options.instructionsText&&(t.$instructionsText=d('<p class="slick-instructions slick-sr-only">'+t.options.instructionsText+"</p>"),t.$instructionsText.prependTo(t.$slider))},l.prototype.buildRows=function(){var i,e,t,o=this,s=document.createDocumentFragment(),n=o.$slider.children();if(0<o.options.rows){for(t=o.options.slidesPerRow*o.options.rows,e=Math.ceil(n.length/t),i=0;i<e;i++){for(var l=document.createElement("div"),r=0;r<o.options.rows;r++){for(var a=document.createElement("div"),d=0;d<o.options.slidesPerRow;d++){var p=i*t+(r*o.options.slidesPerRow+d);n.get(p)&&a.appendChild(n.get(p))}l.appendChild(a)}s.appendChild(l)}o.$slider.empty().append(s),o.$slider.children().children().children().css({width:100/o.options.slidesPerRow+"%",display:"inline-block"})}},l.prototype.checkResponsive=function(i,e){var t,o,s,n=this,l=!1,r=n.$slider.width(),a=window.innerWidth||d(window).width();if("window"===n.respondTo?s=a:"slider"===n.respondTo?s=r:"min"===n.respondTo&&(s=Math.min(a,r)),n.options.responsive&&n.options.responsive.length&&null!==n.options.responsive){for(t in o=null,n.breakpoints)n.breakpoints.hasOwnProperty(t)&&(!1===n.originalSettings.mobileFirst?s<n.breakpoints[t]&&(o=n.breakpoints[t]):s>n.breakpoints[t]&&(o=n.breakpoints[t]));null!==o?null!==n.activeBreakpoint&&o===n.activeBreakpoint&&!e||(n.activeBreakpoint=o,"unslick"===n.breakpointSettings[o]?n.unslick(o):(n.options=d.extend({},n.originalSettings,n.breakpointSettings[o]),!0===i&&(n.currentSlide=n.options.initialSlide),n.refresh(i)),l=o):null!==n.activeBreakpoint&&(n.activeBreakpoint=null,n.options=n.originalSettings,!0===i&&(n.currentSlide=n.options.initialSlide),n.refresh(i),l=o),i||!1===l||n.$slider.trigger("breakpoint",[n,l])}},l.prototype.changeSlide=function(i,e){var t,o,s=this,n=d(i.currentTarget);switch(n.is("a")&&i.preventDefault(),n.is("li")||(n=n.closest("li")),t=s.slideCount%s.options.slidesToScroll!=0?0:(s.slideCount-s.currentSlide)%s.options.slidesToScroll,i.data.message){case"previous":o=0==t?s.options.slidesToScroll:s.options.slidesToShow-t,s.slideCount>s.options.slidesToShow&&s.slideHandler(s.currentSlide-o,!1,e);break;case"next":o=0==t?s.options.slidesToScroll:t,s.slideCount>s.options.slidesToShow&&s.slideHandler(s.currentSlide+o,!1,e);break;case"index":var l=0===i.data.index?0:i.data.index||n.index()*s.options.slidesToScroll;s.slideHandler(s.checkNavigable(l),!1,e),n.children().trigger("focus");break;default:return}},l.prototype.checkNavigable=function(i){var e=this.getNavigableIndexes(),t=0;if(i>e[e.length-1])i=e[e.length-1];else for(var o in e){if(i<e[o]){i=t;break}t=e[o]}return i},l.prototype.cleanUpEvents=function(){var i=this;i.options.autoplay&&i.options.useAutoplayToggleButton&&i.$pauseButton.off("click.slick",i.autoPlayToggleHandler),i.options.dots&&null!==i.$dots&&d("li",i.$dots).off("click.slick",i.changeSlide).off("mouseenter.slick",d.proxy(i.interrupt,i,!0)).off("mouseleave.slick",d.proxy(i.interrupt,i,!1)),i.$slider.off("focus.slick blur.slick"),!0===i.options.arrows&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow&&i.$prevArrow.off("click.slick",i.changeSlide),i.$nextArrow&&i.$nextArrow.off("click.slick",i.changeSlide)),i.$list.off("touchstart.slick mousedown.slick",i.swipeHandler),i.$list.off("touchmove.slick mousemove.slick",i.swipeHandler),i.$list.off("touchend.slick mouseup.slick",i.swipeHandler),i.$list.off("touchcancel.slick mouseleave.slick",i.swipeHandler),i.$list.off("click.slick",i.clickHandler),d(document).off(i.visibilityChange,i.visibility),i.cleanUpSlideEvents(),d(window).off("orientationchange.slick.slick-"+i.instanceUid,i.orientationChange),d(window).off("resize.slick.slick-"+i.instanceUid,i.resize),d("[draggable!=true]",i.$slideTrack).off("dragstart",i.preventDefault),d(window).off("load.slick.slick-"+i.instanceUid,i.setPosition)},l.prototype.cleanUpSlideEvents=function(){var i=this;i.$list.off("mouseenter.slick",d.proxy(i.interrupt,i,!0)),i.$list.off("mouseleave.slick",d.proxy(i.interrupt,i,!1))},l.prototype.cleanUpRows=function(){var i;0<this.options.rows&&((i=this.$slides.children().children()).removeAttr("style"),this.$slider.empty().append(i))},l.prototype.clickHandler=function(i){!1===this.shouldClick&&(i.stopImmediatePropagation(),i.stopPropagation(),i.preventDefault())},l.prototype.destroy=function(i){var e=this;e.autoPlayClear(),e.touchObject={},e.cleanUpEvents(),d(".slick-cloned",e.$slider).detach(),e.options.autoplay&&e.options.useAutoplayToggleButton&&e.$pauseButton.remove(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.$prevArrow.length&&(e.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").prop("disabled",!1).css("display",""),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove()),e.$nextArrow&&e.$nextArrow.length&&(e.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").prop("disabled",!1).css("display",""),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove()),e.$slides&&(e.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){d(this).attr("style",d(this).data("originalStyling"))}),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.detach(),e.$list.detach(),e.$slider.append(e.$slides)),e.cleanUpRows(),e.$slider.removeClass("slick-slider"),e.$slider.removeClass("slick-initialized"),e.$slider.removeClass("slick-dotted"),e.unslicked=!0,i||e.$slider.trigger("destroy",[e])},l.prototype.disableTransition=function(i){var e={};e[this.transitionType]="",!1===this.options.fade?this.$slideTrack.css(e):this.$slides.eq(i).css(e)},l.prototype.fadeSlide=function(i,e){var t=this;!1===t.cssTransitions?(t.$slides.eq(i).css({zIndex:t.options.zIndex}),t.$slides.eq(i).animate({opacity:1},t.options.speed,t.options.easing,e)):(t.applyTransition(i),t.$slides.eq(i).css({opacity:1,zIndex:t.options.zIndex}),e&&setTimeout(function(){t.disableTransition(i),e.call()},t.options.speed))},l.prototype.fadeSlideOut=function(i){var e=this;!1===e.cssTransitions?e.$slides.eq(i).animate({opacity:0,zIndex:e.options.zIndex-2},e.options.speed,e.options.easing):(e.applyTransition(i),e.$slides.eq(i).css({opacity:0,zIndex:e.options.zIndex-2}))},l.prototype.filterSlides=l.prototype.slickFilter=function(i){var e=this;null!==i&&(e.$slidesCache=e.$slides,e.unload(),e.$slideTrack.children(this.options.slide).detach(),e.$slidesCache.filter(i).appendTo(e.$slideTrack),e.reinit())},l.prototype.focusHandler=function(){var t=this;t.$slider.off("focus.slick blur.slick").on("focus.slick","*",function(i){var e=d(this);setTimeout(function(){t.options.pauseOnFocus&&e.is(":focus")&&(t.focussed=!0,t.autoPlay())},0)}).on("blur.slick","*",function(i){d(this);t.options.pauseOnFocus&&(t.focussed=!1,t.autoPlay())})},l.prototype.getCurrent=l.prototype.slickCurrentSlide=function(){return this.currentSlide},l.prototype.getDotCount=function(){var i=this,e=0,t=0,o=0;if(!0===i.options.infinite)if(i.slideCount<=i.options.slidesToShow)++o;else for(;e<i.slideCount;)++o,e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;else if(!0===i.options.centerMode)o=i.slideCount;else if(i.options.asNavFor)for(;e<i.slideCount;)++o,e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;else o=1+Math.ceil((i.slideCount-i.options.slidesToShow)/i.options.slidesToScroll);return o-1},l.prototype.getLeft=function(i){var e,t,o,s,n=this,l=0;return n.slideOffset=0,t=n.$slides.first().outerHeight(!0),!0===n.options.infinite?(n.slideCount>n.options.slidesToShow&&(n.slideOffset=n.slideWidth*n.options.slidesToShow*-1,s=-1,!0===n.options.vertical&&!0===n.options.centerMode&&(2===n.options.slidesToShow?s=-1.5:1===n.options.slidesToShow&&(s=-2)),l=t*n.options.slidesToShow*s),n.slideCount%n.options.slidesToScroll!=0&&i+n.options.slidesToScroll>n.slideCount&&n.slideCount>n.options.slidesToShow&&(l=i>n.slideCount?(n.slideOffset=(n.options.slidesToShow-(i-n.slideCount))*n.slideWidth*-1,(n.options.slidesToShow-(i-n.slideCount))*t*-1):(n.slideOffset=n.slideCount%n.options.slidesToScroll*n.slideWidth*-1,n.slideCount%n.options.slidesToScroll*t*-1))):i+n.options.slidesToShow>n.slideCount&&(n.slideOffset=(i+n.options.slidesToShow-n.slideCount)*n.slideWidth,l=(i+n.options.slidesToShow-n.slideCount)*t),n.slideCount<=n.options.slidesToShow&&(l=n.slideOffset=0),!0===n.options.centerMode&&n.slideCount<=n.options.slidesToShow?n.slideOffset=n.slideWidth*Math.floor(n.options.slidesToShow)/2-n.slideWidth*n.slideCount/2:!0===n.options.centerMode&&!0===n.options.infinite?n.slideOffset+=n.slideWidth*Math.floor(n.options.slidesToShow/2)-n.slideWidth:!0===n.options.centerMode&&(n.slideOffset=0,n.slideOffset+=n.slideWidth*Math.floor(n.options.slidesToShow/2)),e=!1===n.options.vertical?i*n.slideWidth*-1+n.slideOffset:i*t*-1+l,!0===n.options.variableWidth&&(o=n.slideCount<=n.options.slidesToShow||!1===n.options.infinite?n.$slideTrack.children(".slick-slide").eq(i):n.$slideTrack.children(".slick-slide").eq(i+n.options.slidesToShow),e=!0===n.options.rtl?o[0]?-1*(n.$slideTrack.width()-o[0].offsetLeft-o.width()):0:o[0]?-1*o[0].offsetLeft:0,!0===n.options.centerMode&&(o=n.slideCount<=n.options.slidesToShow||!1===n.options.infinite?n.$slideTrack.children(".slick-slide").eq(i):n.$slideTrack.children(".slick-slide").eq(i+n.options.slidesToShow+1),e=!0===n.options.rtl?o[0]?-1*(n.$slideTrack.width()-o[0].offsetLeft-o.width()):0:o[0]?-1*o[0].offsetLeft:0,e+=(n.$list.width()-o.outerWidth())/2)),e},l.prototype.getOption=l.prototype.slickGetOption=function(i){return this.options[i]},l.prototype.getNavigableIndexes=function(){for(var i=this,e=0,t=0,o=[],s=!1===i.options.infinite?i.slideCount:(e=-1*i.options.slidesToScroll,t=-1*i.options.slidesToScroll,2*i.slideCount);e<s;)o.push(e),e=t+i.options.slidesToScroll,t+=i.options.slidesToScroll<=i.options.slidesToShow?i.options.slidesToScroll:i.options.slidesToShow;return o},l.prototype.getSlick=function(){return this},l.prototype.getSlideCount=function(){var s,n=this,i=!0===n.options.centerMode?Math.floor(n.$list.width()/2):0,l=-1*n.swipeLeft+i;return!0===n.options.swipeToSlide?(n.$slideTrack.find(".slick-slide").each(function(i,e){var t=d(e).outerWidth(),o=e.offsetLeft;if(!0!==n.options.centerMode&&(o+=t/2),l<o+t)return s=e,!1}),Math.abs(d(s).attr("data-slick-index")-n.currentSlide)||1):n.options.slidesToScroll},l.prototype.goTo=l.prototype.slickGoTo=function(i,e){this.changeSlide({data:{message:"index",index:parseInt(i)}},e)},l.prototype.init=function(i){var e=this;d(e.$slider).hasClass("slick-initialized")||(d(e.$slider).addClass("slick-initialized"),e.buildRows(),e.buildOut(),e.setProps(),e.startLoad(),e.loadSlider(),e.initializeEvents(),e.updateArrows(),e.updateDots(),e.checkResponsive(!0),e.focusHandler()),i&&e.$slider.trigger("init",[e]),e.options.autoplay&&(e.paused=!1,e.autoPlay()),e.updateSlideVisibility(),null!=e.options.accessibility&&console.warn("accessibility setting is no longer supported."),null!=e.options.focusOnChange&&console.warn("focusOnChange is no longer supported."),null!=e.options.focusOnSelect&&console.warn("focusOnSelect is no longer supported.")},l.prototype.initArrowEvents=function(){var i=this;!0===i.options.arrows&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},i.changeSlide),i.$nextArrow.off("click.slick").on("click.slick",{message:"next"},i.changeSlide))},l.prototype.initDotEvents=function(){var i=this;!0===i.options.dots&&i.slideCount>i.options.slidesToShow&&d("li",i.$dots).on("click.slick",{message:"index"},i.changeSlide),!0===i.options.dots&&!0===i.options.pauseOnDotsHover&&i.slideCount>i.options.slidesToShow&&d("li",i.$dots).on("mouseenter.slick",d.proxy(i.interrupt,i,!0)).on("mouseleave.slick",d.proxy(i.interrupt,i,!1))},l.prototype.initSlideEvents=function(){var i=this;i.options.pauseOnHover&&(i.$list.on("mouseenter.slick",d.proxy(i.interrupt,i,!0)),i.$list.on("mouseleave.slick",d.proxy(i.interrupt,i,!1)))},l.prototype.initializeEvents=function(){var i=this;i.initArrowEvents(),i.initDotEvents(),i.initSlideEvents(),i.options.autoplay&&i.options.useAutoplayToggleButton&&i.$pauseButton.on("click.slick",i.autoPlayToggleHandler),i.$list.on("touchstart.slick mousedown.slick",{action:"start"},i.swipeHandler),i.$list.on("touchmove.slick mousemove.slick",{action:"move"},i.swipeHandler),i.$list.on("touchend.slick mouseup.slick",{action:"end"},i.swipeHandler),i.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},i.swipeHandler),i.$list.on("click.slick",i.clickHandler),d(document).on(i.visibilityChange,d.proxy(i.visibility,i)),d(window).on("orientationchange.slick.slick-"+i.instanceUid,d.proxy(i.orientationChange,i)),d(window).on("resize.slick.slick-"+i.instanceUid,d.proxy(i.resize,i)),d("[draggable!=true]",i.$slideTrack).on("dragstart",i.preventDefault),d(window).on("load.slick.slick-"+i.instanceUid,i.setPosition),d(i.setPosition)},l.prototype.initUI=function(){var i=this;!0===i.options.arrows&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.show(),i.$nextArrow.show()),!0===i.options.dots&&i.slideCount>i.options.slidesToShow&&i.$dots.show()},l.prototype.lazyLoad=function(){var i,e,t,n=this;function o(i){d("img[data-lazy]",i).each(function(){var i=d(this),e=d(this).attr("data-lazy"),t=d(this).attr("data-srcset"),o=d(this).attr("data-sizes")||n.$slider.attr("data-sizes"),s=document.createElement("img");s.onload=function(){i.animate({opacity:0},100,function(){t&&(i.attr("srcset",t),o&&i.attr("sizes",o)),i.attr("src",e).animate({opacity:1},200,function(){i.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")}),n.$slider.trigger("lazyLoaded",[n,i,e])})},s.onerror=function(){i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),n.$slider.trigger("lazyLoadError",[n,i,e])},s.src=e})}if(!0===n.options.centerMode?t=!0===n.options.infinite?(e=n.currentSlide+(n.options.slidesToShow/2+1))+n.options.slidesToShow+2:(e=Math.max(0,n.currentSlide-(n.options.slidesToShow/2+1)),n.options.slidesToShow/2+1+2+n.currentSlide):(e=n.options.infinite?n.options.slidesToShow+n.currentSlide:n.currentSlide,t=Math.ceil(e+n.options.slidesToShow),!0===n.options.fade&&(0<e&&e--,t<=n.slideCount&&t++)),i=n.$slider.find(".slick-slide").slice(e,t),"anticipated"===n.options.lazyLoad)for(var s=e-1,l=t,r=n.$slider.find(".slick-slide"),a=0;a<n.options.slidesToScroll;a++)s<0&&(s=n.slideCount-1),i=(i=i.add(r.eq(s))).add(r.eq(l)),s--,l++;o(i),n.slideCount<=n.options.slidesToShow?o(n.$slider.find(".slick-slide")):n.currentSlide>=n.slideCount-n.options.slidesToShow?o(n.$slider.find(".slick-cloned").slice(0,n.options.slidesToShow)):0===n.currentSlide&&o(n.$slider.find(".slick-cloned").slice(-1*n.options.slidesToShow))},l.prototype.loadSlider=function(){var i=this;i.setPosition(),i.$slideTrack.css({opacity:1}),i.$slider.removeClass("slick-loading"),i.initUI(),"progressive"===i.options.lazyLoad&&i.progressiveLazyLoad()},l.prototype.next=l.prototype.slickNext=function(){this.changeSlide({data:{message:"next"}})},l.prototype.orientationChange=function(){this.checkResponsive(),this.setPosition()},l.prototype.pause=l.prototype.slickPause=function(){this.autoPlayClear(),this.paused=!0},l.prototype.play=l.prototype.slickPlay=function(){var i=this;i.autoPlay(),i.options.autoplay=!0,i.paused=!1,i.focussed=!1,i.interrupted=!1},l.prototype.postSlide=function(i){var e=this;e.unslicked||(e.$slider.trigger("afterChange",[e,i]),e.animating=!1,e.slideCount>e.options.slidesToShow&&e.setPosition(),e.swipeLeft=null,e.options.autoplay&&e.autoPlay(),e.updateSlideVisibility())},l.prototype.prev=l.prototype.slickPrev=function(){this.changeSlide({data:{message:"previous"}})},l.prototype.preventDefault=function(i){i.preventDefault()},l.prototype.progressiveLazyLoad=function(i){i=i||1;var e,t,o,s,n,l=this,r=d("img[data-lazy]",l.$slider);r.length?(e=r.first(),t=e.attr("data-lazy"),o=e.attr("data-srcset"),s=e.attr("data-sizes")||l.$slider.attr("data-sizes"),(n=document.createElement("img")).onload=function(){o&&(e.attr("srcset",o),s&&e.attr("sizes",s)),e.attr("src",t).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"),!0===l.options.adaptiveHeight&&l.setPosition(),l.$slider.trigger("lazyLoaded",[l,e,t]),l.progressiveLazyLoad()},n.onerror=function(){i<3?setTimeout(function(){l.progressiveLazyLoad(i+1)},500):(e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),l.$slider.trigger("lazyLoadError",[l,e,t]),l.progressiveLazyLoad())},n.src=t):l.$slider.trigger("allImagesLoaded",[l])},l.prototype.refresh=function(i){var e,t=this,o=t.slideCount-t.options.slidesToShow;!t.options.infinite&&t.currentSlide>o&&(t.currentSlide=o),t.slideCount<=t.options.slidesToShow&&(t.currentSlide=0),e=t.currentSlide,t.destroy(!0),d.extend(t,t.initials,{currentSlide:e}),t.init(),i||t.changeSlide({data:{message:"index",index:e}},!1)},l.prototype.registerBreakpoints=function(){var i,e,t,o=this,s=o.options.responsive||null;if("array"===d.type(s)&&s.length){for(i in o.respondTo=o.options.respondTo||"window",s)if(t=o.breakpoints.length-1,s.hasOwnProperty(i)){for(e=s[i].breakpoint;0<=t;)o.breakpoints[t]&&o.breakpoints[t]===e&&o.breakpoints.splice(t,1),t--;o.breakpoints.push(e),o.breakpointSettings[e]=s[i].settings}o.breakpoints.sort(function(i,e){return o.options.mobileFirst?i-e:e-i})}},l.prototype.reinit=function(){var i=this;i.$slides=i.$slideTrack.children(i.options.slide).addClass("slick-slide"),i.slideCount=i.$slides.length,i.currentSlide>=i.slideCount&&0!==i.currentSlide&&(i.currentSlide=i.currentSlide-i.options.slidesToScroll),i.slideCount<=i.options.slidesToShow&&(i.currentSlide=0),i.registerBreakpoints(),i.setProps(),i.setupInfinite(),i.buildArrows(),i.updateArrows(),i.initArrowEvents(),i.buildDots(),i.updateDots(),i.initDotEvents(),i.cleanUpSlideEvents(),i.initSlideEvents(),i.checkResponsive(!1,!0),i.setSlideClasses("number"==typeof i.currentSlide?i.currentSlide:0),i.setPosition(),i.focusHandler(),i.paused=!i.options.autoplay,i.autoPlay(),i.$slider.trigger("reInit",[i])},l.prototype.resize=function(){var i=this;d(window).width()!==i.windowWidth&&(clearTimeout(i.windowDelay),i.windowDelay=window.setTimeout(function(){i.windowWidth=d(window).width(),i.checkResponsive(),i.unslicked||i.setPosition()},50))},l.prototype.removeSlide=l.prototype.slickRemove=function(i,e,t){var o=this;if(i="boolean"==typeof i?!0===(e=i)?0:o.slideCount-1:!0===e?--i:i,o.slideCount<1||i<0||i>o.slideCount-1)return!1;o.unload(),!0===t?o.$slideTrack.children().remove():o.$slideTrack.children(this.options.slide).eq(i).remove(),o.$slides=o.$slideTrack.children(this.options.slide),o.$slideTrack.children(this.options.slide).detach(),o.$slideTrack.append(o.$slides),o.$slidesCache=o.$slides,o.reinit()},l.prototype.setCSS=function(i){var e,t,o=this,s={};!0===o.options.rtl&&(i=-i),e="left"==o.positionProp?Math.ceil(i)+"px":"0px",t="top"==o.positionProp?Math.ceil(i)+"px":"0px",s[o.positionProp]=i,!1===o.transformsEnabled||(!(s={})===o.cssTransitions?s[o.animType]="translate("+e+", "+t+")":s[o.animType]="translate3d("+e+", "+t+", 0px)"),o.$slideTrack.css(s)},l.prototype.setDimensions=function(){var i=this;!1===i.options.vertical?!0===i.options.centerMode&&i.$list.css({padding:"0px "+i.options.centerPadding}):(i.$list.height(i.$slides.first().outerHeight(!0)*i.options.slidesToShow),!0===i.options.centerMode&&i.$list.css({padding:i.options.centerPadding+" 0px"})),i.listWidth=i.$list.width(),i.listHeight=i.$list.height(),!1===i.options.vertical&&!1===i.options.variableWidth?(i.slideWidth=Math.ceil(i.listWidth/i.options.slidesToShow),i.$slideTrack.width(Math.ceil(i.slideWidth*i.$slideTrack.children(".slick-slide").length))):!0===i.options.variableWidth?i.$slideTrack.width(5e3*i.slideCount):(i.slideWidth=Math.ceil(i.listWidth),i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0)*i.$slideTrack.children(".slick-slide").length)));var e=i.$slides.first().outerWidth(!0)-i.$slides.first().width();!1===i.options.variableWidth&&i.$slideTrack.children(".slick-slide").width(i.slideWidth-e)},l.prototype.setFade=function(){var t,o=this;o.$slides.each(function(i,e){t=o.slideWidth*i*-1,!0===o.options.rtl?d(e).css({position:"relative",right:t,top:0,zIndex:o.options.zIndex-2,opacity:0}):d(e).css({position:"relative",left:t,top:0,zIndex:o.options.zIndex-2,opacity:0})}),o.$slides.eq(o.currentSlide).css({zIndex:o.options.zIndex-1,opacity:1})},l.prototype.setHeight=function(){var i,e=this;1===e.options.slidesToShow&&!0===e.options.adaptiveHeight&&!1===e.options.vertical&&(i=e.$slides.eq(e.currentSlide).outerHeight(!0),e.$list.css("height",i))},l.prototype.setOption=l.prototype.slickSetOption=function(){var i,e,t,o,s,n=this,l=!1;if("object"===d.type(arguments[0])?(t=arguments[0],l=arguments[1],s="multiple"):"string"===d.type(arguments[0])&&(o=arguments[1],l=arguments[2],"responsive"===(t=arguments[0])&&"array"===d.type(arguments[1])?s="responsive":void 0!==arguments[1]&&(s="single")),"single"===s)n.options[t]=o;else if("multiple"===s)d.each(t,function(i,e){n.options[i]=e});else if("responsive"===s)for(e in o)if("array"!==d.type(n.options.responsive))n.options.responsive=[o[e]];else{for(i=n.options.responsive.length-1;0<=i;)n.options.responsive[i].breakpoint===o[e].breakpoint&&n.options.responsive.splice(i,1),i--;n.options.responsive.push(o[e])}l&&(n.unload(),n.reinit())},l.prototype.setPosition=function(){var i=this;i.setDimensions(),i.setHeight(),!1===i.options.fade?i.setCSS(i.getLeft(i.currentSlide)):i.setFade(),i.$slider.trigger("setPosition",[i])},l.prototype.setProps=function(){var i=this,e=document.body.style;i.positionProp=!0===i.options.vertical?"top":"left","top"===i.positionProp?i.$slider.addClass("slick-vertical"):i.$slider.removeClass("slick-vertical"),void 0===e.WebkitTransition&&void 0===e.MozTransition&&void 0===e.msTransition||!0===i.options.useCSS&&(i.cssTransitions=!0),i.options.fade&&("number"==typeof i.options.zIndex?i.options.zIndex<3&&(i.options.zIndex=3):i.options.zIndex=i.defaults.zIndex),void 0!==e.OTransform&&(i.animType="OTransform",i.transformType="-o-transform",i.transitionType="OTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(i.animType=!1)),void 0!==e.MozTransform&&(i.animType="MozTransform",i.transformType="-moz-transform",i.transitionType="MozTransition",void 0===e.perspectiveProperty&&void 0===e.MozPerspective&&(i.animType=!1)),void 0!==e.webkitTransform&&(i.animType="webkitTransform",i.transformType="-webkit-transform",i.transitionType="webkitTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(i.animType=!1)),void 0!==e.msTransform&&(i.animType="msTransform",i.transformType="-ms-transform",i.transitionType="msTransition",void 0===e.msTransform&&(i.animType=!1)),void 0!==e.transform&&!1!==i.animType&&(i.animType="transform",i.transformType="transform",i.transitionType="transition"),i.transformsEnabled=i.options.useTransform&&null!==i.animType&&!1!==i.animType},l.prototype.setSlideClasses=function(i){var e,t,o,s,n=this,l=n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true").attr("aria-label",function(){return d(this).attr("aria-label").replace(" (centered)","")});n.$slides.eq(i).addClass("slick-current"),!0===n.options.centerMode?(o=n.options.slidesToShow%2==0?1:0,s=Math.floor(n.options.slidesToShow/2),!0===n.options.infinite&&(s<=i&&i<=n.slideCount-1-s?n.$slides.slice(i-s+o,i+s+1).addClass("slick-active").removeAttr("aria-hidden"):(e=n.options.slidesToShow+i,l.slice(e-s+1+o,e+s+2).addClass("slick-active").removeAttr("aria-hidden")),0===i?l.eq(n.options.slidesToShow+n.slideCount+1).addClass("slick-center").attr("aria-label",function(){return d(this).attr("aria-label")+" (centered)"}):i===n.slideCount-1&&l.eq(n.options.slidesToShow).addClass("slick-center").attr("aria-label",function(){return d(this).attr("aria-label")+" (centered)"})),n.$slides.eq(i).addClass("slick-center").attr("aria-label",function(){return d(this).attr("aria-label")+" (centered)"})):0<=i&&i<=n.slideCount-n.options.slidesToShow?n.$slides.slice(i,i+n.options.slidesToShow).addClass("slick-active").removeAttr("aria-hidden"):l.length<=n.options.slidesToShow?l.addClass("slick-active").removeAttr("aria-hidden"):(t=n.slideCount%n.options.slidesToShow,e=!0===n.options.infinite?n.options.slidesToShow+i:i,n.options.slidesToShow==n.options.slidesToScroll&&n.slideCount-i<n.options.slidesToShow?l.slice(e-(n.options.slidesToShow-t),e+t).addClass("slick-active").removeAttr("aria-hidden"):l.slice(e,e+n.options.slidesToShow).addClass("slick-active").removeAttr("aria-hidden")),"ondemand"!==n.options.lazyLoad&&"anticipated"!==n.options.lazyLoad||n.lazyLoad()},l.prototype.setupInfinite=function(){var i,e,t,o=this;if(!0===o.options.fade&&(o.options.centerMode=!1),!0===o.options.infinite&&!1===o.options.fade&&(e=null,o.slideCount>o.options.slidesToShow)){for(t=!0===o.options.centerMode?o.options.slidesToShow+1:o.options.slidesToShow,i=o.slideCount;i>o.slideCount-t;--i)e=i-1,d(o.$slides[e]).clone(!0).attr("id","").attr("data-slick-index",e-o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");for(i=0;i<t+o.slideCount;i+=1)e=i,d(o.$slides[e]).clone(!0).attr("id","").attr("data-slick-index",e+o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");o.$slideTrack.find(".slick-cloned").find("[id]").each(function(){d(this).attr("id","")})}},l.prototype.interrupt=function(i){i||this.autoPlay(),this.interrupted=i},l.prototype.selectHandler=function(i){var e=d(i.target).is(".slick-slide")?d(i.target):d(i.target).parents(".slick-slide"),t=(t=parseInt(e.attr("data-slick-index")))||0;this.slideCount<=this.options.slidesToShow?this.slideHandler(t,!1,!0):this.slideHandler(t)},l.prototype.slideHandler=function(i,e,t){var o,s,n,l,r,a,d=this;if(e=e||!1,!(!0===d.animating&&!0===d.options.waitForAnimate||!0===d.options.fade&&d.currentSlide===i))if(!1===e&&d.asNavFor(i),o=i,r=d.getLeft(o),l=d.getLeft(d.currentSlide),d.currentLeft=null===d.swipeLeft?l:d.swipeLeft,!1===d.options.infinite&&!1===d.options.centerMode&&(i<0||i>d.getDotCount()*d.options.slidesToScroll))!1===d.options.fade&&(o=d.currentSlide,!0!==t&&d.slideCount>d.options.slidesToShow?d.animateSlide(l,function(){d.postSlide(o)}):d.postSlide(o));else if(!1===d.options.infinite&&!0===d.options.centerMode&&(i<0||i>d.slideCount-d.options.slidesToScroll))!1===d.options.fade&&(o=d.currentSlide,!0!==t&&d.slideCount>d.options.slidesToShow?d.animateSlide(l,function(){d.postSlide(o)}):d.postSlide(o));else{if(d.options.autoplay&&clearInterval(d.autoPlayTimer),s=o<0?d.slideCount%d.options.slidesToScroll!=0?d.slideCount-d.slideCount%d.options.slidesToScroll:d.slideCount+o:o>=d.slideCount?d.slideCount%d.options.slidesToScroll!=0?0:o-d.slideCount:o,d.animating=!0,d.$slider.trigger("beforeChange",[d,d.currentSlide,s]),n=d.currentSlide,d.currentSlide=s,d.setSlideClasses(d.currentSlide),d.options.asNavFor&&(a=(a=d.getNavTarget()).slick("getSlick")).slideCount<=a.options.slidesToShow&&a.setSlideClasses(d.currentSlide),d.updateDots(),d.updateArrows(),!0===d.options.fade)return!0!==t?(d.fadeSlideOut(n),d.fadeSlide(s,function(){d.postSlide(s)})):d.postSlide(s),void d.animateHeight();!0!==t&&d.slideCount>d.options.slidesToShow?d.animateSlide(r,function(){d.postSlide(s)}):d.postSlide(s)}},l.prototype.startLoad=function(){var i=this;!0===i.options.arrows&&i.slideCount>i.options.slidesToShow&&(i.$prevArrow.hide(),i.$nextArrow.hide()),!0===i.options.dots&&i.slideCount>i.options.slidesToShow&&i.$dots.hide(),i.$slider.addClass("slick-loading")},l.prototype.swipeDirection=function(){var i=this,e=i.touchObject.startX-i.touchObject.curX,t=i.touchObject.startY-i.touchObject.curY,o=Math.atan2(t,e),s=Math.round(180*o/Math.PI);return s<0&&(s=360-Math.abs(s)),s<=45&&0<=s||s<=360&&315<=s?!1===i.options.rtl?"left":"right":135<=s&&s<=225?!1===i.options.rtl?"right":"left":!0===i.options.verticalSwiping?35<=s&&s<=135?"down":"up":"vertical"},l.prototype.swipeEnd=function(i){var e,t,o=this;if(o.dragging=!1,o.swiping=!1,o.scrolling)return o.scrolling=!1;if(o.interrupted=!1,o.shouldClick=!(10<o.touchObject.swipeLength),void 0===o.touchObject.curX)return!1;if(!0===o.touchObject.edgeHit&&o.$slider.trigger("edge",[o,o.swipeDirection()]),o.touchObject.swipeLength>=o.touchObject.minSwipe){switch(t=o.swipeDirection()){case"left":case"down":e=o.options.swipeToSlide?o.checkNavigable(o.currentSlide+o.getSlideCount()):o.currentSlide+o.getSlideCount(),o.currentDirection=0;break;case"right":case"up":e=o.options.swipeToSlide?o.checkNavigable(o.currentSlide-o.getSlideCount()):o.currentSlide-o.getSlideCount(),o.currentDirection=1}"vertical"!=t&&(o.slideHandler(e),o.touchObject={},o.$slider.trigger("swipe",[o,t]))}else o.touchObject.startX!==o.touchObject.curX&&(o.slideHandler(o.currentSlide),o.touchObject={})},l.prototype.swipeHandler=function(i){var e=this;if(!(!1===e.options.swipe||"ontouchend"in document&&!1===e.options.swipe||!1===e.options.draggable&&-1!==i.type.indexOf("mouse")))switch(e.touchObject.fingerCount=i.originalEvent&&void 0!==i.originalEvent.touches?i.originalEvent.touches.length:1,e.touchObject.minSwipe=e.listWidth/e.options.touchThreshold,!0===e.options.verticalSwiping&&(e.touchObject.minSwipe=e.listHeight/e.options.touchThreshold),i.data.action){case"start":e.swipeStart(i);break;case"move":e.swipeMove(i);break;case"end":e.swipeEnd(i)}},l.prototype.swipeMove=function(i){var e,t,o,s,n,l=this,r=void 0!==i.originalEvent?i.originalEvent.touches:null;return!(!l.dragging||l.scrolling||r&&1!==r.length)&&(e=l.getLeft(l.currentSlide),l.touchObject.curX=void 0!==r?r[0].pageX:i.clientX,l.touchObject.curY=void 0!==r?r[0].pageY:i.clientY,l.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(l.touchObject.curX-l.touchObject.startX,2))),n=Math.round(Math.sqrt(Math.pow(l.touchObject.curY-l.touchObject.startY,2))),!l.options.verticalSwiping&&!l.swiping&&4<n?!(l.scrolling=!0):(!0===l.options.verticalSwiping&&(l.touchObject.swipeLength=n),t=l.swipeDirection(),void 0!==i.originalEvent&&4<l.touchObject.swipeLength&&(l.swiping=!0,i.preventDefault()),s=(!1===l.options.rtl?1:-1)*(l.touchObject.curX>l.touchObject.startX?1:-1),!0===l.options.verticalSwiping&&(s=l.touchObject.curY>l.touchObject.startY?1:-1),o=l.touchObject.swipeLength,(l.touchObject.edgeHit=!1)===l.options.infinite&&(0===l.currentSlide&&"right"===t||l.currentSlide>=l.getDotCount()&&"left"===t)&&(o=l.touchObject.swipeLength*l.options.edgeFriction,l.touchObject.edgeHit=!0),!1===l.options.vertical?l.swipeLeft=e+o*s:l.swipeLeft=e+o*(l.$list.height()/l.listWidth)*s,!0===l.options.verticalSwiping&&(l.swipeLeft=e+o*s),!0!==l.options.fade&&!1!==l.options.touchMove&&(!0===l.animating?(l.swipeLeft=null,!1):void l.setCSS(l.swipeLeft))))},l.prototype.swipeStart=function(i){var e,t=this;if(t.interrupted=!0,1!==t.touchObject.fingerCount||t.slideCount<=t.options.slidesToShow)return!(t.touchObject={});void 0!==i.originalEvent&&void 0!==i.originalEvent.touches&&(e=i.originalEvent.touches[0]),t.touchObject.startX=t.touchObject.curX=void 0!==e?e.pageX:i.clientX,t.touchObject.startY=t.touchObject.curY=void 0!==e?e.pageY:i.clientY,t.dragging=!0},l.prototype.unfilterSlides=l.prototype.slickUnfilter=function(){var i=this;null!==i.$slidesCache&&(i.unload(),i.$slideTrack.children(this.options.slide).detach(),i.$slidesCache.appendTo(i.$slideTrack),i.reinit())},l.prototype.unload=function(){var i=this;d(".slick-cloned",i.$slider).remove(),i.$dots&&i.$dots.remove(),i.$prevArrow&&i.htmlExpr.test(i.options.prevArrow)&&i.$prevArrow.remove(),i.$nextArrow&&i.htmlExpr.test(i.options.nextArrow)&&i.$nextArrow.remove(),i.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},l.prototype.unslick=function(i){this.$slider.trigger("unslick",[this,i]),this.destroy()},l.prototype.updateArrows=function(){var i=this;Math.floor(i.options.slidesToShow/2);!0===i.options.arrows&&i.slideCount>i.options.slidesToShow&&!i.options.infinite&&(i.$prevArrow.removeClass("slick-disabled").prop("disabled",!1),i.$nextArrow.removeClass("slick-disabled").prop("disabled",!1),0===i.currentSlide?(i.$prevArrow.addClass("slick-disabled").prop("disabled",!0),i.$nextArrow.removeClass("slick-disabled").prop("disabled",!1)):(i.currentSlide>=i.slideCount-i.options.slidesToShow&&!1===i.options.centerMode||i.currentSlide>=i.slideCount-1&&!0===i.options.centerMode)&&(i.$nextArrow.addClass("slick-disabled").prop("disabled",!0),i.$prevArrow.removeClass("slick-disabled").prop("disabled",!1)))},l.prototype.updateDots=function(){var i=this;null!==i.$dots&&(i.$dots.find("li").removeClass("slick-active").find("button").removeAttr("aria-current").end().end(),i.$dots.find("li").eq(Math.floor(i.currentSlide/i.options.slidesToScroll)).addClass("slick-active").find("button").attr("aria-current",!0).end().end())},l.prototype.updateSlideVisibility=function(){this.$slideTrack.find(".slick-slide").attr("aria-hidden","true").find("a, input, button, select").attr("tabindex","-1"),this.$slideTrack.find(".slick-active").removeAttr("aria-hidden").find("a, input, button, select").removeAttr("tabindex")},l.prototype.visibility=function(){this.options.autoplay&&(document[this.hidden]?this.interrupted=!0:this.interrupted=!1)},d.fn.slick=function(){for(var i,e=this,t=arguments[0],o=Array.prototype.slice.call(arguments,1),s=e.length,n=0;n<s;n++)if("object"==typeof t||void 0===t?e[n].slick=new l(e[n],t):i=e[n].slick[t].apply(e[n].slick,o),void 0!==i)return i;return e}});

// cumbria-specific js
window.addEventListener("DOMContentLoaded", () => {
    // quote carousel
    if($('.carousel-quote-desc-container').length) {
        let quoteArray = []
        // set carousel container
        for(let i = 0; i < ($('.carousel-quote-desc-container').length); i++) {
            quoteArray[i] = new A11YSlider(document.querySelectorAll('.carousel-quote-desc-container')[i], {
                adaptiveHeight: false,
                dots: false,
                container: false,
                swipe: true,
                skipBtn: true,
                swipe: true,
                slidesToShow: 1
            });
        }
    }

    // blocks carousel
    if($('.carousel-blocks-content').length) {
        let blocksArray = []
        // set carousel container
        for(let i = 0; i < ($('.carousel-blocks-content').length); i++) {
            // for blocks of 2, only show max of 2 and add class to provide smaller width to containing element
            if(document.querySelectorAll('.carousel-blocks-content')[i].getElementsByTagName('li').length <= 2) {
                document.querySelectorAll('.carousel-blocks-content')[i].classList.add('two-items')
                blocksArray[i] = new A11YSlider(document.querySelectorAll('.carousel-blocks-content')[i], {
                adaptiveHeight: false,
                dots: false,
                container: false,
                swipe: true,
                skipBtn: true,
                swipe: true,
                slidesToShow: 1,
                responsive: {
                    500: {
                        slidesToShow: 2
                    },
                    700: {
                        slidesToShow: 2
                    },
                    900: {
                        slidesToShow: 2
                    }
                }
            })} else {
                if(document.querySelectorAll('.carousel-blocks-content')[i].classList.contains('images-only')) { 
                    if(document.querySelectorAll('.carousel-blocks-content')[i].getElementsByTagName('li').length <= 3) {
                        // if carousel is image only, increase number of slidesToShow
                        blocksArray[i] = new A11YSlider(document.querySelectorAll('.carousel-blocks-content')[i], {
                            adaptiveHeight: false,
                            dots: false,
                            container: false,
                            swipe: true,
                            skipBtn: true,
                            swipe: true,
                            slidesToShow: 2,
                            responsive: {
                                500: {
                                    slidesToShow: 3
                                },
                                700: {
                                    slidesToShow: 3
                                },
                                900: {
                                    slidesToShow: 3
                                }
                            }
                        }) 
                    } else if(document.querySelectorAll('.carousel-blocks-content')[i].getElementsByTagName('li').length == 4) {
                        // if carousel is image only, increase number of slidesToShow
                        blocksArray[i] = new A11YSlider(document.querySelectorAll('.carousel-blocks-content')[i], {
                            adaptiveHeight: false,
                            dots: false,
                            container: false,
                            swipe: true,
                            skipBtn: true,
                            swipe: true,
                            slidesToShow: 2,
                            responsive: {
                                500: {
                                    slidesToShow: 3
                                },
                                700: {
                                    slidesToShow: 4
                                },
                                900: {
                                    slidesToShow: 4
                                }
                            }
                        }) 
                    } else {
                        // if carousel is image only, increase number of slidesToShow
                        blocksArray[i] = new A11YSlider(document.querySelectorAll('.carousel-blocks-content')[i], {
                            adaptiveHeight: false,
                            dots: false,
                            container: false,
                            swipe: true,
                            skipBtn: true,
                            swipe: true,
                            slidesToShow: 2,
                            responsive: {
                                500: {
                                    slidesToShow: 3
                                },
                                700: {
                                    slidesToShow: 4
                                },
                                900: {
                                    slidesToShow: 5
                                }
                            }
                        }) 
                    }
                } else {
                    // for blocks of 3 or more
                    blocksArray[i] = new A11YSlider(document.querySelectorAll('.carousel-blocks-content')[i], {
                    adaptiveHeight: false,
                    dots: false,
                    container: false,
                    swipe: true,
                    skipBtn: true,
                    swipe: true,
                    slidesToShow: 1,
                    responsive: {
                        500: {
                            slidesToShow: 2
                        },
                        700: {
                            slidesToShow: 3
                        },
                        900: {
                            slidesToShow: 3
                        }
                    }
                })}
            }
        }
    }
    
    // news events carousel
    if($('.carousel-news-events-content').length) {
        // set carousel container
        let newsArray = []
        // set carousel container
        for(let i = 0; i < ($('.carousel-news-events-content').length); i++) {
            newsArray[i] = new A11YSlider(document.querySelectorAll('.carousel-news-events-content')[i], {
                adaptiveHeight: true,
                dots: false,
                container: false,
                swipe: true,
                skipBtn: true,
                swipe: true,
                slidesToShow: 1,
                responsive: {
                    500: {
                        slidesToShow: 2
                    },
                    700: {
                        slidesToShow: 3
                    },
                    900: {
                        slidesToShow: 3
                    }
                }
            });
        }
    }

    // image and description carousel
    if($('.carousel-image-desc-container').length) {
        // set carousel container
        let imageDescArray = []
        // set carousel container
        for(let i = 0; i < ($('.carousel-image-desc-container').length); i++) {
            // for blocks of 2, only show max of 2 and add class to provide smaller width to containing element
            if(document.querySelectorAll('.carousel-image-desc-container')[i].getElementsByTagName('li').length <= 2) {
                document.querySelectorAll('.carousel-image-desc-container')[i].classList.add('two-items')
                imageDescArray[i] = new A11YSlider(document.querySelectorAll('.carousel-image-desc-container')[i], {
                    adaptiveHeight: false,
                    dots: false,
                    container: false,
                    swipe: true,
                    skipBtn: true,
                    swipe: true,
                    slidesToShow: 1,
                    responsive: {
                        500: {
                            slidesToShow: 2
                        },
                        700: {
                            slidesToShow: 2
                        },
                        900: {
                            slidesToShow: 2
                        }
                    }
                })
            } else if(document.querySelectorAll('.carousel-image-desc-container')[i].getElementsByTagName('li').length == 3) {
                document.querySelectorAll('.carousel-image-desc-container')[i].classList.add('three-items')
                imageDescArray[i] = new A11YSlider(document.querySelectorAll('.carousel-image-desc-container')[i], {
                    adaptiveHeight: false,
                    dots: false,
                    container: false,
                    swipe: true,
                    skipBtn: true,
                    swipe: true,
                    slidesToShow: 1,
                    responsive: {
                        500: {
                            slidesToShow: 2
                        },
                        700: {
                            slidesToShow: 3
                        },
                        900: {
                            slidesToShow: 3
                        }
                    }
                })
            } else {
                imageDescArray[i] = new A11YSlider(document.querySelectorAll('.carousel-image-desc-container')[i], {
                    adaptiveHeight: false,
                    dots: false,
                    container: false,
                    swipe: true,
                    skipBtn: true,
                    swipe: true,
                    slidesToShow: 1,
                    responsive: {
                        500: {
                            slidesToShow: 2
                        },
                        700: {
                            slidesToShow: 3
                        },
                        900: {
                            slidesToShow: 4
                        }
                    }
                });
            }
        }
    }

    // image only carousel
    if($('.carousel-image-only-fade-container').length) {
        let imageCarouselArray = []
        // set carousel container
        for(let i = 0; i < ($('.carousel-image-only-fade-container').length); i++) {
            // initialise slider
            $('div.carousel-image-only-fade-container').eq(i).slick({
                slidesToShow: 3,
                dots: false,
                responsive: [
                {
                    breakpoint: 900,
                    settings: {
                    slidesToShow: 1,
                    centerMode: true
                    }
                }
                ]
            });
        }
    }

    // spotlight carousel
    if($('.carousel-spotlight-content').length) {
        for(let i = 0; i < ($('.carousel-spotlight-content').length); i++) {
            // set images and text carousel containers
            const spotlightCarouselContainerImages = document.querySelectorAll('.carousel-spotlight-images')[i]
            const spotlightCarouselContainerText = document.querySelectorAll('.carousel-spotlight-content')[i]
            const spotlightCarouselContainerControls = document.querySelectorAll('.carousel-spotlight-content-controls')[i]

            // initialise both sliders - one for the images, one for the content. One is set to mimic the other so only need one set of buttons to control both
            const spotlightCarouselImages = new A11YSlider(spotlightCarouselContainerImages, {
                adaptiveHeight: false,
                dots: false,
                container: false,
                swipe: true,
                skipBtn: false, // 'skip this carousel' button disabled due to two containers fused together - doesn't skip both so wouldn't skip whole carousel
                prevArrow: spotlightCarouselContainerControls.getElementsByClassName('spotlightPrevButton'),
                nextArrow: spotlightCarouselContainerControls.getElementsByClassName('spotlightNextButton'),
                swipe: true
            });
            const spotlightCarouselText = new A11YSlider(spotlightCarouselContainerText, {
                adaptiveHeight: true,
                dots: false,
                container: false,
                arrows: false,
                skipBtn: false,
                swipe: true
            });

            // set content slider to listen to images slider for user input and copy - so both sliders move as one carousel
            spotlightCarouselContainerImages.addEventListener("beforeChange", function(e) {
                // Get next element
                const nextSlide = e.detail.nextSlide;
                // Get index of the next element
                const nextSlideIndex = Array.from(nextSlide.parentNode.children).indexOf(nextSlide);
                // Tell 2nd slider to move to slide element based off index
                spotlightCarouselText.scrollToSlide(nextSlideIndex);
            });

            // find height of title box currently shown and move slider control buttons to middle of box
            var spotlightCarouselButtonVerticalAlign = function (info) {

                if(document.querySelectorAll('.carousel-spotlight-content-title').length > 1) {
                    let newHeight = document.querySelectorAll('.a11y-slider-active .carousel-spotlight-content-title')[i].offsetHeight;
                    spotlightCarouselContainerControls.style.height = `${newHeight}px`;
                } else {
                    document.querySelector('.carousel-spotlight-content-controls').style.display = 'none';
                    document.querySelector('.carousel-spotlight-content-title').classList.add('no-arrows');
                }

            }

            spotlightCarouselContainerImages.addEventListener("afterChange", function(e) {
                spotlightCarouselButtonVerticalAlign();
            });

            skipCarousel = function() {
                // 13 == enter key, 32 == spacebar
                return (event.keyCode != 13 && event.keyCode != 32) || document.getElementById('carousel-skipped').focus();
            }

            $(document).ready(function(){
                // set up the vertical alignment of slider buttons when font loaded and page ready
                document.fonts.ready.then(function () {
                    spotlightCarouselButtonVerticalAlign()
                });
                // if slider exists, set vertical height of slider buttons in case webfonts don't load
                if(spotlightCarouselText) {
                    spotlightCarouselButtonVerticalAlign()
                }
                // reset vertical alignment on window resize
                $(window).resize(function() {
                    spotlightCarouselButtonVerticalAlign()
                });
            });
        }
    }
    
    // tab panel
    if($('[role="tab"]').length) {
        const tabs = document.querySelectorAll('[role="tab"]');
        const tabList = document.querySelector('[role="tablist"]');
    
        // Add a click event handler to each tab
        tabs.forEach(tab => {
            tab.addEventListener("click", changeTabs);
        });
    
        // Enable arrow navigation between tabs in the tab list
        let tabFocus = 0;
    
        tabList.addEventListener("keydown", e => {
        // Move right
        if (e.keyCode === 39 || e.keyCode === 37) {
            tabs[tabFocus].setAttribute("tabindex", -1);
            if (e.keyCode === 39) {
                tabFocus++;
                // If we're at the end, go to the start
                if (tabFocus >= tabs.length) {
                    tabFocus = 0;
                }
                // Move left
                } else if (e.keyCode === 37) {
                    tabFocus--;
                    // If we're at the start, move to the end
                    if (tabFocus < 0) {
                        tabFocus = tabs.length - 1;
                    }
                }
        
                tabs[tabFocus].setAttribute("tabindex", 0);
                tabs[tabFocus].focus();
            }
        });
        
        function changeTabs(e) {
            const target = e.target;
            const parent = target.parentNode.parentNode;
            const grandparent = parent.parentNode.parentNode.parentNode;
        
            // Remove all current selected tabs
            parent
            .querySelectorAll('[aria-selected="true"]')
            .forEach(t => t.setAttribute("aria-selected", false));
        
            // Set this tab as selected
            target.setAttribute("aria-selected", true);
        
            // Hide all tab panels and remove all tabindex
            grandparent
            .querySelectorAll('[role="tabpanel"]')
            .forEach(p => {
                p.setAttribute("hidden", true)
                p.setAttribute("tabindex", "-1")
                return
            });
        
            // Show the selected panel
            grandparent
            .querySelector(`#${target.getAttribute("aria-controls")}`)
            .removeAttribute("hidden");
            // Add keyboard control to panel
            grandparent
            .querySelector(`#${target.getAttribute("aria-controls")}`)
            .setAttribute("tabindex", "0")
        }
    }

    // blog and news scroll nav bar
    if($('.blog-nav-mobile').length) {
        var c, currentScrollTop = 0,
        navbar = $('.blog-nav-mobile');

        $(window).scroll(function () {
            var a = $(window).scrollTop();
            var b = navbar.height();
            
            currentScrollTop = a;
            
            if (c < currentScrollTop && a > b + b) {
                navbar.removeClass("scrollUp");
            } else if (c > currentScrollTop && !(a <= b)) {
                navbar.addClass("scrollUp");
            }
            c = currentScrollTop;
        });
    }

    // scroll nav bar
    let showScrollNav = true;

    $('.close-banner-button').on('click', function() {
        $('.scroll-nav').hide();
        showScrollNav = false;
    })

    if($('.scroll-nav').length && showScrollNav) {
        var c, currentScrollTop = 0,
        navbar = $('.scroll-nav');

        $(window).scroll(function () {
            var a = $(window).scrollTop();
            var b = navbar.height();
            
            currentScrollTop = a;
            
            if (c < currentScrollTop && a > b + b) {
                navbar.removeClass("scrollUp");
            } else if (c > currentScrollTop && !(a <= b)) {
                navbar.addClass("scrollUp");
            }
            c = currentScrollTop;
        });
    }

    // day countdown timer
    function startTimer(target, display) {
        var timer = target;
        setInterval(function () {
            let now = new Date().getTime();
            let timeLeft = target - now;

            let days = (Math.floor(timeLeft / (1000 * 60 * 60 * 24)))+1;
            if(days < 1) {
                display.textContent = '';
            } else if (days == 1){
                display.textContent = `${days} day`;
            } else {
                display.textContent = `${days} days`;
            }
    
            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    window.onload = function () {
        if(document.querySelector('#day-countdown') !== null) {
            var display = document.querySelector('#day-countdown'),
                targetDate = new Date("Jun 10, 2021 00:00:00").getTime();
            startTimer(targetDate, display);
        }
    };

    (function(){
        // video header controls
        if(document.getElementById("video-header")) {
            // get width of device window (more reliable than browser width)
            let screenWidth = window.screen.availWidth

            const videoImage = document.getElementById("video-image")
            const video = document.getElementById("video-header")

            // check if video is to play on mobile
            const playMobile = video.classList.contains('play-mobile')

            // if source(s) exist, set video srcs based on device width else set as default data-src
            let sources = { default: { src: video.getAttribute('data-src') }}
            if(video.querySelector('[data-src]')) {
                video.querySelectorAll('[data-src]').forEach(source => sources[source.getAttribute('data-size')] = { src: source.getAttribute('data-src') })
                video.innerHTML = '';
                if(screenWidth < 700) video.src = sources.mobile.src
                else video.src = sources.default.src
            } else {
                video.src = sources.default.src
            }

            const playButton = document.getElementById("video-play")
            const pauseButton = document.getElementById("video-pause")
            const volumeButton = document.getElementById("video-volume")
            const playButtonMobile = document.getElementById("video-play-mobile")
            const pauseButtonMobile = document.getElementById("video-pause-mobile")

            // check if svg shapes exist
            const shapesExist = document.getElementById("video-shapes")
            let shapes 
            if(shapesExist) shapes = document.getElementById("video-shapes")

            // check if video h1 exists
            let videoHeader
            let videoHeaderExists = document.getElementById("video-text-header")
            if(videoHeaderExists) videoHeader = document.getElementById("video-text-header")

            // check if additional text over video exists
            let videoText
            let videoTextExists = document.getElementById("video-text")
            if(videoTextExists) videoText = document.getElementById("video-text")

            const heroWrapper = document.getElementById("video-wrapper")
            const heroControls = document.getElementById("video-controls")

            // check if video contain needs to change size between when video playing and when paused image showing
            let changeVideoSize = true
            if(heroWrapper.classList.contains('no-change-in-size')) changeVideoSize = false

            const originalWrapperHeight = heroWrapper.style.height
            const originalWrapperMaxHeight = heroWrapper.style.maxHeight

            let metadataFired = false
            let canPlayFired = false
            let videoPaused = true
            let firstPlay = true
            let videoRatio = '0'

            const prefersReducedMotion = function() {
                // if users browser supports reduced motion and the user has set it to true, remove the autoplay attribute from the video and pause it
                if(matchMedia("(prefers-reduced-motion)").matches) {
                    video.removeAttribute("autoplay");
                    pauseVideo(pause);
                }
            }

            const findVideoRatio = function(height, width) {
                return (height / width)
            }

            const showControls = function(value) {
                heroControls.style.opacity = (value ? '1' : '0')
            }

            const playVideo = function(sound) {
                videoRatio = findVideoRatio(video.videoHeight, video.videoWidth)

                // set height of wrapper if change in video size needed
                if(changeVideoSize) {
                    heroWrapper.style.height = (videoImage.width  * videoRatio ) + "px"
                    heroWrapper.style.maxHeight = "90vh"
                }

                // if other items exist then remove from view
                videoImage.style.opacity = "0";
                if(videoHeaderExists) videoHeader.style.opacity = "0";
                if(videoTextExists) videoText.style.opacity = "0";
                if(shapesExist) shapes.style.opacity = "0";

                // muted on autoplay first play
                if(sound || !firstPlay) video.muted = false
                else video.muted = true

                firstPlay = false

                showControls(false)
                videoPaused = false

                video.play()
            }

            const playVideoMobile = function(sound) {
                videoRatio = findVideoRatio(video.videoHeight, video.videoWidth)

                // set height and width of wrapper to mobile video size
                heroWrapper.style.width = screenWidth + "px"
                heroWrapper.style.height = (screenWidth * videoRatio) + "px"
                video.style.width = screenWidth + "px"
                heroWrapper.style.height = (video.style.width * videoRatio ) + "px"
                videoImage.style.opacity = "0";

                // if other items exist then remove from view
                if(videoHeaderExists) videoHeader.style.opacity = "0";
                if(videoTextExists) videoText.style.opacity = "0";
                if(shapesExist) shapes.style.opacity = "0";

                // toggle play/pause button display
                playButtonMobile.style.opacity = "0";
                playButtonMobile.style.zIndex = "0"
                pauseButtonMobile.style.opacity = "1"
                pauseButtonMobile.style.zIndex = "1"

                if(sound) video.muted = false
                else video.muted = true

                videoPaused = false
                video.play()
            }

            const pauseVideo = function(pause) {
                // set height of wrapper back to original if needed
                if(changeVideoSize) {
                    heroWrapper.style.height = originalWrapperHeight
                    heroWrapper.style.maxHeight = originalWrapperMaxHeight
                }

                // check if other items exist and show if so
                videoImage.style.opacity = "1";
                if(videoHeaderExists) videoHeader.style.opacity = "1";
                if(videoTextExists) videoText.style.opacity = "1";
                if(shapesExist) shapes.style.opacity = "1";

                // for mobile toggle play/pause buttons
                if(screenWidth <= 700) {
                    playButtonMobile.style.opacity = "1"
                    playButtonMobile.style.zIndex = "1"
                    pauseButtonMobile.style.opacity = "0"
                    pauseButtonMobile.style.zIndex = "0"
                }

                showControls(true)
                videoPaused = true

                if(pause) video.pause();
            }

            // video start/end listeners
            video.addEventListener("loadedmetadata", function() {
                metadataFired = true
                if(metadataFired && canPlayFired) {
                    if(screenWidth > 700) playVideo(false)
                    else {
                        if(playMobile) playVideoMobile(false)
                        else {
                            pauseButtonMobile.style.zIndex = "0"
                            playButtonMobile.style.zIndex = "1"
                        }
                    }
                }
            })

            video.addEventListener("canplay", function() {
                canPlayFired = true
                if(metadataFired && canPlayFired) {
                    if(screenWidth > 700) playVideo(false)
                    else {
                        if(playMobile) playVideoMobile(false)
                        else {
                            pauseButtonMobile.style.zIndex = "0"
                            playButtonMobile.style.zIndex = "1"
                        }
                    }
                }
            })

            video.addEventListener("ended", function() {
                pauseVideo(false)
            })
            video.addEventListener("play", function() {
                if(videoPaused) {
                    if(screenWidth > 700) {
                        playVideo()
                    } else {
                        playVideoMobile()
                    }
                }
            })
            video.addEventListener("pause", function() {
                pauseVideo(false)
            })

            // video control listeners
            heroControls.addEventListener("mouseover", function() {
                showControls(true)
            })
            playButton.addEventListener("focus", function() {
                showControls(true)
            })
            pauseButton.addEventListener("focus", function() {
                showControls(true)
            })
            volumeButton.addEventListener("focus", function() {
                showControls(true)
            })

            heroControls.addEventListener("mouseout", function() {
                if(!videoPaused) showControls(false)
            })

            playButton.addEventListener("click", function() {
                if(video.paused == true) playVideo(true);
            })

            pauseButton.addEventListener("click", function() {
                if(video.paused == false) pauseVideo(true)
            })

            volumeButton.addEventListener("click", function() {
                video.muted = !video.muted;
            })

            playButtonMobile.addEventListener("click", function() {
                playVideoMobile(true)
            })

            pauseButtonMobile.addEventListener("click", function() {
                if(video.paused == false) pauseVideo(true)
            })

            // for rotating devices during video play
            window.addEventListener("orientationchange", function() {
                screenWidth = window.screen.availWidth
                if(videoPaused) {
                    pauseVideo()
                } else {
                    if(screenWidth > 700) {
                        playVideo()
                    } else {
                        playVideoMobile()
                    }
                }
            });
            window.addEventListener("resize", function() {
                screenWidth = window.screen.availWidth
                if(videoPaused) {
                    pauseVideo()
                } else {
                    if(screenWidth > 700) {
                        playVideo()
                    } else {
                        playVideoMobile()
                    }
                }
            });

            // keyboard focus
            playButton.addEventListener("focus", showControls(true))
            pauseButton.addEventListener("focus", showControls(true))
            volumeButton.addEventListener("focus", showControls(true))

            prefersReducedMotion()
        }

        // listings embed
        if(document.querySelectorAll('.embed-listings-content')[0]) {
            let container = document.querySelectorAll('.listings-container')[0]
            // find number of items in container
            // if less than 4 then handle without carousel
            if(container.querySelectorAll('a.single-listing').length <= 3) {
                document.querySelectorAll('.listings-container')[0].className += ' non-carousel-items'

                // add classes for less than 3 items
                if(container.querySelectorAll('a.single-listing').length == 2) {
                    document.querySelectorAll('.listings-container')[0].className += ' two-items'
                }
                if(container.querySelectorAll('a.single-listing').length == 1) {
                    document.querySelectorAll('.listings-container')[0].className += ' one-item'
                }
                // if no items then hide the listings container, button and show a message
                if(container.querySelectorAll('a.single-listing').length == 0) {
                    document.querySelectorAll('.listings-container')[0].style.display = 'none'
                    document.querySelectorAll('.post-listings-button')[0].style.display = 'none'
                    document.getElementById('listing-empty-message-embed').className += 'show-message'
                }
            }
            // if 4 or more, start carousel
            else {
                if($('.listings-container').length) {
                    // set carousel container
                    let itemsArray = []
                    // set carousel container
                    for(let i = 0; i < ($('.listings-container').length); i++) {
                        document.querySelectorAll('.listings-container')[i].className += ' carousel-items'
                        itemsArray[i] = new A11YSlider(document.querySelectorAll('.listings-container')[i], {
                            adaptiveHeight: false,
                            dots: false,
                            container: true,
                            swipe: true,
                            skipBtn: true,
                            swipe: true,
                            slidesToShow: 1,
                            responsive: {
                                500: {
                                    slidesToShow: 1
                                },
                                700: {
                                    slidesToShow: 2
                                },
                                900: {
                                    slidesToShow: 2
                                },
                                1300: {
                                    slidesToShow: 3
                                }
                            }
                        });
                    }
                }
            }
        } 
    })();

    // accordian
    (function () {
        "use strict";
        $(".aks-accordion-item-icon, .aks-accordion-item-title").click(function () {
          var parentAccordion = $(this).closest("[data-accordion-item]");
          parentAccordion.toggleClass("opened");
          let pressed = parentAccordion.children(".aks-accordion-item-row").children(".aks-accordion-item-icon").attr("aria-pressed") === "true";
          let expanded = parentAccordion.children(".aks-accordion-item-content").attr("aria-expanded") === "true";
          let hide = parentAccordion.children(".aks-accordion-item-content").attr("aria-hidden") === "true";
          parentAccordion.children(".aks-accordion-item-row").children(".aks-accordion-item-icon").attr("aria-pressed", !pressed)
          parentAccordion.children(".aks-accordion-item-content").attr("aria-expanded", !expanded)
          parentAccordion.children(".aks-accordion-item-content").attr("aria-hidden", !hide)
          parentAccordion.children(".aks-accordion-item-content").slideToggle("slow");
        });
        $(".aks-accordion-item-icon, .aks-accordion-item-title").keydown(function (e) {
            if(e.which === 13 || e.which === 32) {
                var parentAccordion = $(this).closest("[data-accordion-item]");
                parentAccordion.toggleClass("opened");
                let pressed = parentAccordion.children(".aks-accordion-item-row").children(".aks-accordion-item-icon").attr("aria-pressed") === "true";let expanded = parentAccordion.children(".aks-accordion-item-content").attr("aria-expanded") === "true";
                let hide = parentAccordion.children(".aks-accordion-item-content").attr("aria-hidden") === "true";
                parentAccordion.children(".aks-accordion-item-row").children(".aks-accordion-item-icon").attr("aria-pressed", !pressed)
                parentAccordion.children(".aks-accordion-item-content").attr("aria-expanded", !expanded)
                parentAccordion.children(".aks-accordion-item-content").attr("aria-hidden", !hide)
                parentAccordion.children(".aks-accordion-item-content").slideToggle("slow");
            }
        });
    })();

    let panelOpen = false;

    $('.listings-filter-close a').on("click", function(e) {
        e.preventDefault()
        if(!panelOpen) {
            $('.listings-filter-extra-options-container-wrapper').slideDown("slow", function() {
                $('.showPanel').text("Less options")
                panelOpen = true
                $('.listings-filter-close a').attr('aria-expanded', "true")
            })
        } else {
            $('.listings-filter-extra-options-container-wrapper').slideUp("slow", function() { 
                $('.showPanel').text("More options")
                panelOpen = false
                $('.listings-filter-close a').attr('aria-expanded', "false")
            })
        }
    })

    $('body').on('keydown', '.listings-filter-close a', function (e) {
        if(e.which === 13 || e.which === 32) {
            e.preventDefault()
            if(!panelOpen) {
                $('.listings-filter-extra-options-container-wrapper').slideDown("slow", function() {
                    $('.showPanel').text("Less options")
                    panelOpen = true
                    $('.listings-filter-close a').attr('aria-expanded', "true")
                })
            } else {
                $('.listings-filter-extra-options-container-wrapper').slideUp("slow", function() { 
                    $('.showPanel').text("More options")
                    panelOpen = false
                    $('.listings-filter-close a').attr('aria-expanded', "false")
                })
            }
        }
    })
});

$('.fetch-grid-load-more').click(function () {
    $(this).addClass('btn-loading');
    $(this).text("Loading");
    $.ajax({
        url: $(this).attr('data-all-posts'),
        type: 'GET',
        success: function (data) {
            setTimeout(function () {
                $('.btn-loading').closest('section').find('.ajax-elm').html($(data).find('#content-to-fetch').html());
                $('.lazy').Lazy();
            }, 1200);
        }
    });
});

/**
 * A lightweight youtube embed. Still should feel the same to the user, just MUCH faster to initialize and paint.
 *
 * Thx to these as the inspiration
 *   https://storage.googleapis.com/amp-vs-non-amp/youtube-lazy.html
 *   https://autoplay-youtube-player.glitch.me/
 *
 * Once built it, I also found these:
 *   https://github.com/ampproject/amphtml/blob/master/extensions/amp-youtube (👍👍)
 *   https://github.com/Daugilas/lazyYT
 *   https://github.com/vb/lazyframe
 */
 class LiteYTEmbed extends HTMLElement {
    connectedCallback() {
        this.videoId = this.getAttribute('videoid');

        let playBtnEl = this.querySelector('.lty-playbtn');
        // A label for the button takes priority over a [playlabel] attribute on the custom-element
        this.playLabel = (playBtnEl && playBtnEl.textContent.trim()) || this.getAttribute('playlabel') || 'Play';
        this.screenWidth = this.offsetWidth
        this.posterRes = this.getAttribute('posterres') || 'hqdefault'

        if(this.getAttribute('posterres')) this.posterRes = this.getAttribute('posterres')
        if(this.screenWidth > 320) this.posterRes = 'hqdefault'
        if(this.screenWidth > 480) this.posterRes = 'sddefault'
        if(this.screenWidth > 640) this.posterRes = 'maxresdefault'

        /**
         * Lo, the youtube placeholder image!  (aka the thumbnail, poster image, etc)
         *
         * See https://github.com/paulirish/lite-youtube-embed/blob/master/youtube-thumbnail-urls.md
         *
         * TODO: Do the sddefault->hqdefault fallback
         *       - When doing this, apply referrerpolicy (https://github.com/ampproject/amphtml/pull/3940)
         * TODO: Consider using webp if supported, falling back to jpg
         */
        if (!this.style.backgroundImage) {
          this.posterUrl = `https://i.ytimg.com/vi/${this.videoId}/${this.posterRes}.jpg`;
          // Warm the connection for the poster image
          LiteYTEmbed.addPrefetch('preload', this.posterUrl, 'image');

          this.style.backgroundImage = `url("${this.posterUrl}")`;
        }

        // Set up play button, and its visually hidden label
        if (!playBtnEl) {
            playBtnEl = document.createElement('button');
            playBtnEl.type = 'button';
            playBtnEl.classList.add('lty-playbtn');
            this.append(playBtnEl);
        }
        if (!playBtnEl.textContent) {
            const playBtnLabelEl = document.createElement('span');
            playBtnLabelEl.className = 'lyt-visually-hidden';
            playBtnLabelEl.textContent = this.playLabel;
            playBtnEl.append(playBtnLabelEl);
        }

        // On hover (or tap), warm up the TCP connections we're (likely) about to use.
        this.addEventListener('pointerover', LiteYTEmbed.warmConnections, {once: true});

        // Once the user clicks, add the real iframe and drop our play button
        // TODO: In the future we could be like amp-youtube and silently swap in the iframe during idle time
        //   We'd want to only do this for in-viewport or near-viewport ones: https://github.com/ampproject/amphtml/pull/5003
        this.addEventListener('click', e => this.addIframe());
    }

    // // TODO: Support the the user changing the [videoid] attribute
    // attributeChangedCallback() {
    // }

    /**
     * Add a <link rel={preload | preconnect} ...> to the head
     */
    static addPrefetch(kind, url, as) {
        const linkEl = document.createElement('link');
        linkEl.rel = kind;
        linkEl.href = url;
        if (as) {
            linkEl.as = as;
        }
        document.head.append(linkEl);
    }

    /**
     * Begin pre-connecting to warm up the iframe load
     * Since the embed's network requests load within its iframe,
     *   preload/prefetch'ing them outside the iframe will only cause double-downloads.
     * So, the best we can do is warm up a few connections to origins that are in the critical path.
     *
     * Maybe `<link rel=preload as=document>` would work, but it's unsupported: http://crbug.com/593267
     * But TBH, I don't think it'll happen soon with Site Isolation and split caches adding serious complexity.
     */
    static warmConnections() {
        if (LiteYTEmbed.preconnected) return;

        // The iframe document and most of its subresources come right off youtube.com
        LiteYTEmbed.addPrefetch('preconnect', 'https://www.youtube-nocookie.com');
        // The botguard script is fetched off from google.com
        LiteYTEmbed.addPrefetch('preconnect', 'https://www.google.com');

        // Not certain if these ad related domains are in the critical path. Could verify with domain-specific throttling.
        LiteYTEmbed.addPrefetch('preconnect', 'https://googleads.g.doubleclick.net');
        LiteYTEmbed.addPrefetch('preconnect', 'https://static.doubleclick.net');

        LiteYTEmbed.preconnected = true;
    }

    addIframe() {
        const params = new URLSearchParams(this.getAttribute('params') || []);
        params.append('autoplay', '1');

        const iframeEl = document.createElement('iframe');
        iframeEl.width = 560;
        iframeEl.height = 315;
        // No encoding necessary as [title] is safe. https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html#:~:text=Safe%20HTML%20Attributes%20include
        iframeEl.title = this.playLabel;
        iframeEl.allow = 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture';
        iframeEl.allowFullscreen = true;
        // AFAIK, the encoding here isn't necessary for XSS, but we'll do it only because this is a URL
        // https://stackoverflow.com/q/64959723/89484
        iframeEl.src = `https://www.youtube-nocookie.com/embed/${encodeURIComponent(this.videoId)}?${params.toString()}`;
        this.append(iframeEl);

        this.classList.add('lyt-activated');

        // Set focus for a11y
        this.querySelector('iframe').focus();
    }
}
// Register custom element
customElements.define('lite-youtube', LiteYTEmbed);


/*!
 * jQuery & Zepto Lazy - v1.7.10
 * http://jquery.eisbehr.de/lazy/
 *
 * Copyright 2012 - 2018, Daniel 'Eisbehr' Kern
 *
 * Dual licensed under the MIT and GPL-2.0 licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * $("img.lazy").lazy();
 */

;(function(window, undefined) {
    "use strict";

    // noinspection JSUnresolvedVariable
    /**
     * library instance - here and not in construct to be shorter in minimization
     * @return void
     */
    var $ = window.jQuery || window.Zepto,

    /**
     * unique plugin instance id counter
     * @type {number}
     */
    lazyInstanceId = 0,

    /**
     * helper to register window load for jQuery 3
     * @type {boolean}
     */    
    windowLoaded = false;

    /**
     * make lazy available to jquery - and make it a bit more case-insensitive :)
     * @access public
     * @type {function}
     * @param {object} settings
     * @return {LazyPlugin}
     */
    $.fn.Lazy = $.fn.lazy = function(settings) {
        return new LazyPlugin(this, settings);
    };

    /**
     * helper to add plugins to lazy prototype configuration
     * @access public
     * @type {function}
     * @param {string|Array} names
     * @param {string|Array|function} [elements]
     * @param {function} loader
     * @return void
     */
    $.Lazy = $.lazy = function(names, elements, loader) {
        // make second parameter optional
        if ($.isFunction(elements)) {
            loader = elements;
            elements = [];
        }

        // exit here if parameter is not a callable function
        if (!$.isFunction(loader)) {
            return;
        }

        // make parameters an array of names to be sure
        names = $.isArray(names) ? names : [names];
        elements = $.isArray(elements) ? elements : [elements];

        var config = LazyPlugin.prototype.config,
            forced = config._f || (config._f = {});

        // add the loader plugin for every name
        for (var i = 0, l = names.length; i < l; i++) {
            if (config[names[i]] === undefined || $.isFunction(config[names[i]])) {
                config[names[i]] = loader;
            }
        }

        // add forced elements loader
        for (var c = 0, a = elements.length; c < a; c++) {
            forced[elements[c]] = names[0];
        }
    };

    /**
     * contains all logic and the whole element handling
     * is packed in a private function outside class to reduce memory usage, because it will not be created on every plugin instance
     * @access private
     * @type {function}
     * @param {LazyPlugin} instance
     * @param {object} config
     * @param {object|Array} items
     * @param {object} events
     * @param {string} namespace
     * @return void
     */
    function _executeLazy(instance, config, items, events, namespace) {
        /**
         * a helper to trigger the 'onFinishedAll' callback after all other events
         * @access private
         * @type {number}
         */
        var _awaitingAfterLoad = 0,

        /**
         * visible content width
         * @access private
         * @type {number}
         */
        _actualWidth = -1,

        /**
         * visible content height
         * @access private
         * @type {number}
         */
        _actualHeight = -1,

        /**
         * determine possibly detected high pixel density
         * @access private
         * @type {boolean}
         */
        _isRetinaDisplay = false, 

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _afterLoad = 'afterLoad',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _load = 'load',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _error = 'error',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _img = 'img',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _src = 'src',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _srcset = 'srcset',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _sizes = 'sizes',

        /**
         * dictionary entry for better minimization
         * @access private
         * @type {string}
         */
        _backgroundImage = 'background-image';

        /**
         * initialize plugin
         * bind loading to events or set delay time to load all items at once
         * @access private
         * @return void
         */
        function _initialize() {
            // detect actual device pixel ratio
            // noinspection JSUnresolvedVariable
            _isRetinaDisplay = window.devicePixelRatio > 1;

            // prepare all initial items
            items = _prepareItems(items);

            // if delay time is set load all items at once after delay time
            if (config.delay >= 0) {
                setTimeout(function() {
                    _lazyLoadItems(true);
                }, config.delay);
            }

            // if no delay is set or combine usage is active bind events
            if (config.delay < 0 || config.combined) {
                // create unique event function
                events.e = _throttle(config.throttle, function(event) {
                    // reset detected window size on resize event
                    if (event.type === 'resize') {
                        _actualWidth = _actualHeight = -1;
                    }

                    // execute 'lazy magic'
                    _lazyLoadItems(event.all);
                });

                // create function to add new items to instance
                events.a = function(additionalItems) {
                    additionalItems = _prepareItems(additionalItems);
                    items.push.apply(items, additionalItems);
                };

                // create function to get all instance items left
                events.g = function() {
                    // filter loaded items before return in case internal filter was not running until now
                    return (items = $(items).filter(function() {
                        return !$(this).data(config.loadedName);
                    }));
                };

                // create function to force loading elements
                events.f = function(forcedItems) {
                    for (var i = 0; i < forcedItems.length; i++) {
                        // only handle item if available in current instance
                        // use a compare function, because Zepto can't handle object parameter for filter
                        // var item = items.filter(forcedItems[i]);
                        /* jshint loopfunc: true */
                        var item = items.filter(function() {
                            return this === forcedItems[i];
                        });

                        if (item.length) {
                            _lazyLoadItems(false, item);   
                        }
                    }
                };

                // load initial items
                _lazyLoadItems();

                // bind lazy load functions to scroll and resize event
                // noinspection JSUnresolvedVariable
                $(config.appendScroll).on('scroll.' + namespace + ' resize.' + namespace, events.e);
            }
        }

        /**
         * prepare items before handle them
         * @access private
         * @param {Array|object|jQuery} items
         * @return {Array|object|jQuery}
         */
        function _prepareItems(items) {
            // fetch used configurations before loops
            var defaultImage = config.defaultImage,
                placeholder = config.placeholder,
                imageBase = config.imageBase,
                srcsetAttribute = config.srcsetAttribute,
                loaderAttribute = config.loaderAttribute,
                forcedTags = config._f || {};

            // filter items and only add those who not handled yet and got needed attributes available
            items = $(items).filter(function() {
                var element = $(this),
                    tag = _getElementTagName(this);

                return !element.data(config.handledName) && 
                       (element.attr(config.attribute) || element.attr(srcsetAttribute) || element.attr(loaderAttribute) || forcedTags[tag] !== undefined);
            })

            // append plugin instance to all elements
            .data('plugin_' + config.name, instance);

            for (var i = 0, l = items.length; i < l; i++) {
                var element = $(items[i]),
                    tag = _getElementTagName(items[i]),
                    elementImageBase = element.attr(config.imageBaseAttribute) || imageBase;

                // generate and update source set if an image base is set
                if (tag === _img && elementImageBase && element.attr(srcsetAttribute)) {
                    element.attr(srcsetAttribute, _getCorrectedSrcSet(element.attr(srcsetAttribute), elementImageBase));
                }

                // add loader to forced element types
                if (forcedTags[tag] !== undefined && !element.attr(loaderAttribute)) {
                    element.attr(loaderAttribute, forcedTags[tag]);
                }

                // set default image on every element without source
                if (tag === _img && defaultImage && !element.attr(_src)) {
                    element.attr(_src, defaultImage);
                }

                // set placeholder on every element without background image
                else if (tag !== _img && placeholder && (!element.css(_backgroundImage) || element.css(_backgroundImage) === 'none')) {
                    element.css(_backgroundImage, "url('" + placeholder + "')");
                }
            }

            return items;
        }

        /**
         * the 'lazy magic' - check all items
         * @access private
         * @param {boolean} [allItems]
         * @param {object} [forced]
         * @return void
         */
        function _lazyLoadItems(allItems, forced) {
            // skip if no items where left
            if (!items.length) {
                // destroy instance if option is enabled
                if (config.autoDestroy) {
                    // noinspection JSUnresolvedFunction
                    instance.destroy();
                }

                return;
            }

            var elements = forced || items,
                loadTriggered = false,
                imageBase = config.imageBase || '',
                srcsetAttribute = config.srcsetAttribute,
                handledName = config.handledName;

            // loop all available items
            for (var i = 0; i < elements.length; i++) {
                // item is at least in loadable area
                if (allItems || forced || _isInLoadableArea(elements[i])) {
                    var element = $(elements[i]),
                        tag = _getElementTagName(elements[i]),
                        attribute = element.attr(config.attribute),
                        elementImageBase = element.attr(config.imageBaseAttribute) || imageBase,
                        customLoader = element.attr(config.loaderAttribute);

                        // is not already handled 
                    if (!element.data(handledName) &&
                        // and is visible or visibility doesn't matter
                        (!config.visibleOnly || element.is(':visible')) && (
                        // and image source or source set attribute is available
                        (attribute || element.attr(srcsetAttribute)) && (
                            // and is image tag where attribute is not equal source or source set
                            (tag === _img && (elementImageBase + attribute !== element.attr(_src) || element.attr(srcsetAttribute) !== element.attr(_srcset))) ||
                            // or is non image tag where attribute is not equal background
                            (tag !== _img && elementImageBase + attribute !== element.css(_backgroundImage))
                        ) ||
                        // or custom loader is available
                        customLoader))
                    {
                        // mark element always as handled as this point to prevent double handling
                        loadTriggered = true;
                        element.data(handledName, true);

                        // load item
                        _handleItem(element, tag, elementImageBase, customLoader);
                    }
                }
            }

            // when something was loaded remove them from remaining items
            if (loadTriggered) {
                items = $(items).filter(function() {
                    return !$(this).data(handledName);
                });
            }
        }

        /**
         * load the given element the lazy way
         * @access private
         * @param {object} element
         * @param {string} tag
         * @param {string} imageBase
         * @param {function} [customLoader]
         * @return void
         */
        function _handleItem(element, tag, imageBase, customLoader) {
            // increment count of items waiting for after load
            ++_awaitingAfterLoad;

            // extended error callback for correct 'onFinishedAll' handling
            var errorCallback = function() {
                _triggerCallback('onError', element);
                _reduceAwaiting();

                // prevent further callback calls
                errorCallback = $.noop;
            };

            // trigger function before loading image
            _triggerCallback('beforeLoad', element);

            // fetch all double used data here for better code minimization
            var srcAttribute = config.attribute,
                srcsetAttribute = config.srcsetAttribute,
                sizesAttribute = config.sizesAttribute,
                retinaAttribute = config.retinaAttribute,
                removeAttribute = config.removeAttribute,
                loadedName = config.loadedName,
                elementRetina = element.attr(retinaAttribute);

            // handle custom loader
            if (customLoader) {
                // on load callback
                var loadCallback = function() {
                    // remove attribute from element
                    if (removeAttribute) {
                        element.removeAttr(config.loaderAttribute);
                    }

                    // mark element as loaded
                    element.data(loadedName, true);

                    // call after load event
                    _triggerCallback(_afterLoad, element);

                    // remove item from waiting queue and possibly trigger finished event
                    // it's needed to be asynchronous to run after filter was in _lazyLoadItems
                    setTimeout(_reduceAwaiting, 1);

                    // prevent further callback calls
                    loadCallback = $.noop;
                };

                // bind error event to trigger callback and reduce waiting amount
                element.off(_error).one(_error, errorCallback)

                // bind after load callback to element
                .one(_load, loadCallback);

                // trigger custom loader and handle response
                if (!_triggerCallback(customLoader, element, function(response) {
                    if(response) {
                        element.off(_load);
                        loadCallback();
                    }
                    else {
                        element.off(_error);
                        errorCallback();
                    }
                })) {
                    element.trigger(_error);
                }
            }

            // handle images
            else {
                // create image object
                var imageObj = $(new Image());

                // bind error event to trigger callback and reduce waiting amount
                imageObj.one(_error, errorCallback)

                // bind after load callback to image
                .one(_load, function() {
                    // remove element from view
                    element.hide();

                    // set image back to element
                    // do it as single 'attr' calls, to be sure 'src' is set after 'srcset'
                    if (tag === _img) {
                        element.attr(_sizes, imageObj.attr(_sizes))
                               .attr(_srcset, imageObj.attr(_srcset))
                               .attr(_src, imageObj.attr(_src));
                    }
                    else {
                        element.css(_backgroundImage, "url('" + imageObj.attr(_src) + "')");
                    }

                    // bring it back with some effect!
                    element[config.effect](config.effectTime);

                    // remove attribute from element
                    if (removeAttribute) {
                        element.removeAttr(srcAttribute + ' ' + srcsetAttribute + ' ' + retinaAttribute + ' ' + config.imageBaseAttribute);

                        // only remove 'sizes' attribute, if it was a custom one
                        if (sizesAttribute !== _sizes) {
                            element.removeAttr(sizesAttribute);
                        }
                    }

                    // mark element as loaded
                    element.data(loadedName, true);

                    // call after load event
                    _triggerCallback(_afterLoad, element);

                    // cleanup image object
                    imageObj.remove();

                    // remove item from waiting queue and possibly trigger finished event
                    _reduceAwaiting();
                });

                // set sources
                // do it as single 'attr' calls, to be sure 'src' is set after 'srcset'
                var imageSrc = (_isRetinaDisplay && elementRetina ? elementRetina : element.attr(srcAttribute)) || '';
                imageObj.attr(_sizes, element.attr(sizesAttribute))
                        .attr(_srcset, element.attr(srcsetAttribute))
                        .attr(_src, imageSrc ? imageBase + imageSrc : null);

                // call after load even on cached image
                imageObj.complete && imageObj.trigger(_load); // jshint ignore : line
            }
        }

        /**
         * check if the given element is inside the current viewport or threshold
         * @access private
         * @param {object} element
         * @return {boolean}
         */
        function _isInLoadableArea(element) {
            var elementBound = element.getBoundingClientRect(),
                direction    = config.scrollDirection,
                threshold    = config.threshold,
                vertical     = // check if element is in loadable area from top
                               ((_getActualHeight() + threshold) > elementBound.top) &&
                               // check if element is even in loadable are from bottom
                               (-threshold < elementBound.bottom),
                horizontal   = // check if element is in loadable area from left
                               ((_getActualWidth() + threshold) > elementBound.left) &&
                               // check if element is even in loadable area from right
                               (-threshold < elementBound.right);

            if (direction === 'vertical') {
                return vertical;
            }
            else if (direction === 'horizontal') {
                return horizontal;
            }

            return vertical && horizontal;
        }

        /**
         * receive the current viewed width of the browser
         * @access private
         * @return {number}
         */
        function _getActualWidth() {
            return _actualWidth >= 0 ? _actualWidth : (_actualWidth = $(window).width());
        }

        /**
         * receive the current viewed height of the browser
         * @access private
         * @return {number}
         */
        function _getActualHeight() {
            return _actualHeight >= 0 ? _actualHeight : (_actualHeight = $(window).height());
        }

        /**
         * get lowercase tag name of an element
         * @access private
         * @param {object} element
         * @returns {string}
         */
        function _getElementTagName(element) {
            return element.tagName.toLowerCase();
        }

        /**
         * prepend image base to all srcset entries
         * @access private
         * @param {string} srcset
         * @param {string} imageBase
         * @returns {string}
         */
        function _getCorrectedSrcSet(srcset, imageBase) {
            if (imageBase) {
                // trim, remove unnecessary spaces and split entries
                var entries = srcset.split(',');
                srcset = '';

                for (var i = 0, l = entries.length; i < l; i++) {
                    srcset += imageBase + entries[i].trim() + (i !== l - 1 ? ',' : '');
                }
            }

            return srcset;
        }

        /**
         * helper function to throttle down event triggering
         * @access private
         * @param {number} delay
         * @param {function} callback
         * @return {function}
         */
        function _throttle(delay, callback) {
            var timeout,
                lastExecute = 0;

            return function(event, ignoreThrottle) {
                var elapsed = +new Date() - lastExecute;

                function run() {
                    lastExecute = +new Date();
                    // noinspection JSUnresolvedFunction
                    callback.call(instance, event);
                }

                timeout && clearTimeout(timeout); // jshint ignore : line

                if (elapsed > delay || !config.enableThrottle || ignoreThrottle) {
                    run();
                }
                else {
                    timeout = setTimeout(run, delay - elapsed);
                }
            };
        }

        /**
         * reduce count of awaiting elements to 'afterLoad' event and fire 'onFinishedAll' if reached zero
         * @access private
         * @return void
         */
        function _reduceAwaiting() {
            --_awaitingAfterLoad;

            // if no items were left trigger finished event
            if (!items.length && !_awaitingAfterLoad) {
                _triggerCallback('onFinishedAll');
            }
        }

        /**
         * single implementation to handle callbacks, pass element and set 'this' to current instance
         * @access private
         * @param {string|function} callback
         * @param {object} [element]
         * @param {*} [args]
         * @return {boolean}
         */
        function _triggerCallback(callback, element, args) {
            if ((callback = config[callback])) {
                // jQuery's internal '$(arguments).slice(1)' are causing problems at least on old iPads
                // below is shorthand of 'Array.prototype.slice.call(arguments, 1)'
                callback.apply(instance, [].slice.call(arguments, 1));
                return true;
            }

            return false;
        }

        // if event driven or window is already loaded don't wait for page loading
        if (config.bind === 'event' || windowLoaded) {
            _initialize();
        }

        // otherwise load initial items and start lazy after page load
        else {
            // noinspection JSUnresolvedVariable
            $(window).on(_load + '.' + namespace, _initialize);
        }  
    }

    /**
     * lazy plugin class constructor
     * @constructor
     * @access private
     * @param {object} elements
     * @param {object} settings
     * @return {object|LazyPlugin}
     */
    function LazyPlugin(elements, settings) {
        /**
         * this lazy plugin instance
         * @access private
         * @type {object|LazyPlugin|LazyPlugin.prototype}
         */
        var _instance = this,

        /**
         * this lazy plugin instance configuration
         * @access private
         * @type {object}
         */
        _config = $.extend({}, _instance.config, settings),

        /**
         * instance generated event executed on container scroll or resize
         * packed in an object to be referenceable and short named because properties will not be minified
         * @access private
         * @type {object}
         */
        _events = {},

        /**
         * unique namespace for instance related events
         * @access private
         * @type {string}
         */
        _namespace = _config.name + '-' + (++lazyInstanceId);

        // noinspection JSUndefinedPropertyAssignment
        /**
         * wrapper to get or set an entry from plugin instance configuration
         * much smaller on minify as direct access
         * @access public
         * @type {function}
         * @param {string} entryName
         * @param {*} [value]
         * @return {LazyPlugin|*}
         */
        _instance.config = function(entryName, value) {
            if (value === undefined) {
                return _config[entryName];
            }

            _config[entryName] = value;
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * add additional items to current instance
         * @access public
         * @param {Array|object|string} items
         * @return {LazyPlugin}
         */
        _instance.addItems = function(items) {
            _events.a && _events.a($.type(items) === 'string' ? $(items) : items); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * get all left items of this instance
         * @access public
         * @returns {object}
         */
        _instance.getItems = function() {
            return _events.g ? _events.g() : {};
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force lazy to load all items in loadable area right now
         * by default without throttle
         * @access public
         * @type {function}
         * @param {boolean} [useThrottle]
         * @return {LazyPlugin}
         */
        _instance.update = function(useThrottle) {
            _events.e && _events.e({}, !useThrottle); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force element(s) to load directly, ignoring the viewport
         * @access public
         * @param {Array|object|string} items
         * @return {LazyPlugin}
         */
        _instance.force = function(items) {
            _events.f && _events.f($.type(items) === 'string' ? $(items) : items); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * force lazy to load all available items right now
         * this call ignores throttling
         * @access public
         * @type {function}
         * @return {LazyPlugin}
         */
        _instance.loadAll = function() {
            _events.e && _events.e({all: true}, true); // jshint ignore : line
            return _instance;
        };

        // noinspection JSUndefinedPropertyAssignment
        /**
         * destroy this plugin instance
         * @access public
         * @type {function}
         * @return undefined
         */
        _instance.destroy = function() {
            // unbind instance generated events
            // noinspection JSUnresolvedFunction, JSUnresolvedVariable
            $(_config.appendScroll).off('.' + _namespace, _events.e);
            // noinspection JSUnresolvedVariable
            $(window).off('.' + _namespace);

            // clear events
            _events = {};

            return undefined;
        };

        // start using lazy and return all elements to be chainable or instance for further use
        // noinspection JSUnresolvedVariable
        _executeLazy(_instance, _config, elements, _events, _namespace);
        return _config.chainable ? elements : _instance;
    }

    /**
     * settings and configuration data
     * @access public
     * @type {object|*}
     */
    LazyPlugin.prototype.config = {
        // general
        name               : 'lazy',
        chainable          : true,
        autoDestroy        : true,
        bind               : 'load',
        threshold          : 500,
        visibleOnly        : false,
        appendScroll       : window,
        scrollDirection    : 'both',
        imageBase          : null,
        defaultImage       : 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
        placeholder        : null,
        delay              : -1,
        combined           : false,

        // attributes
        attribute          : 'data-src',
        srcsetAttribute    : 'data-srcset',
        sizesAttribute     : 'data-sizes',
        retinaAttribute    : 'data-retina',
        loaderAttribute    : 'data-loader',
        imageBaseAttribute : 'data-imagebase',
        removeAttribute    : true,
        handledName        : 'handled',
        loadedName         : 'loaded',

        // effect
        effect             : 'show',
        effectTime         : 0,

        // throttle
        enableThrottle     : true,
        throttle           : 250,

        // callbacks
        beforeLoad         : undefined,
        afterLoad          : undefined,
        onError            : undefined,
        onFinishedAll      : undefined
    };

    // register window load event globally to prevent not loading elements
    // since jQuery 3.X ready state is fully async and may be executed after 'load' 
    $(window).on('load', function() {
        windowLoaded = true;
    });
})(window);

// Google Tag Manager
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PGGZPB');